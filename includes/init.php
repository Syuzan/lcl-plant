<?php

session_start();
error_reporting(E_ALL);

include 'includes/config.php';
include 'includes/functions.php';
include 'includes/password.php';

function __autoload ($class_name) {
	include 'classes/'.$class_name.'.php';
}
$db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

$user = new User($db);
$user->setIsSignedIn();

?>
