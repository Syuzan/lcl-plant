<?php

function redirect ($url) {
	header('Location: '.$url);
	die();
}

function sanitize (&$value) {
	$value = htmlspecialchars(trim($value), ENT_QUOTES);
}

function desanitize (&$value) {
	$value = htmlspecialchars_decode(trim($value), ENT_QUOTES);
}

?>