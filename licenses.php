<?php

include 'includes/init.php';

if (!$user->is_signed_in || $user->level != 'Admin') {
	redirect('index.php');
}

$page['title'] = 'Licenses';

include 'header.php';

?>
<div class="page-header">
	<a href="ajax.php?table=qualification&method=modal&action=create_license" title="Create License" class="btn btn-primary pull-right modal-link"><i class="fa fa-plus"></i></a>
	<h1><?php echo $page['title']; ?></h1>
</div>

<?php

$lo = new LicensesOutput($db, $user);
$lo->fetchRows();
$lo->showTable();

include 'footer.php';
?>