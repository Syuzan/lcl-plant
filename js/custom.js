$(document).ready(function() {
	// Send email with attachments
	$('.error_msg').hide();
	setTimeout(function(){ $('.error_msg').hide(); }, 5000);
	$(document).on('click', '.activate_email', function () {
		$('.choose_img,.email_div').toggle();
		$('.popup_td_div4').toggleClass('sml_width');
	})
	$(document).on('click', '.show_images', function () {
		$('.choose_img,.email_div').hide();
	})
	$(document).on('click','.send_email',function(){
		var form = new FormData($('#sendEmail_form')[0]);
		var images = $('.choose_img').val();
		var _this = $(this);
		var email = $('.email_to');
		var reg = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
		if (!reg.test(email.val()) || email.val() == "")
		{
		    $('.error_msg').text('Please enter valid email address').show();
		}else{
		    $.ajax({
				method:"POST",
				url: 'ajax.php?table=qualification&method=modal&action=send_email',
				data: form,
				contentType: false,
				processData: false,
				success: function(data){
					var result = JSON.parse(data);
					if(result.success == 'send'){
						$('.error_msg').text('Email has been sent').show();
					}else{
						$('.error_msg').text('Please try again').show();
					}
				}
		    });
		}
	})
	$(document).on('hidden.bs.modal', '.modal', function(event) {
		$(this).remove();
	});

	// when submit clicked, basic validation
	$(document).on('click', 'button[type=submit]', function (e) {
		$errors = false;
		$form = $(this).closest('form');
		$form.attr('novalidate', true);
		
		$form.find('[required]').each(function() {
			if ($(this).val().length == 0) {
				$(this).closest('.form-group').addClass('has-error');
				$errors = true;
			}
			else {
				$(this).closest('.form-group').removeClass('has-error');				
			}
		});
		
		if ($errors) {
			alert('Please complete the required fields highlighted in red.');
			e.preventDefault();
		}
	});
	
	// when required field blurred
	$(document).on('blur change', '[required]', function () {
		if ($(this).val().length == 0) {
			$(this).closest('.form-group').addClass('has-error');
		}
		else {
			$(this).closest('.form-group').removeClass('has-error');			
		}
	});
	
	// show modal
	$(document).on('click', '.modal-link', function (e) {
		e.preventDefault();

		$.get($(this).attr('href'), function (data) {
			if (data == 'no permission') {
				no_permission_alert();
			}
			else {
				$(data).modal('show');
			}
		});
	});

	// submit create/update modal form

	$(document).on('submit', '.modal-form', function (e) {
		e.preventDefault();

		$action = $(this).attr('action');
		$table = $.url($action).param('table');
		$method = $.url($action).param('method');
		
		$.post($action, $(this).serialize()).done(function (data) {
			if (data == 'no permission') {
				no_permission_alert();
			}
			else {
				if ($('#row-empty').length) {
					$('#row-empty').hide();
				}
				
				if ($method == 'create' || $method == 'create_lic') {
					$('#table-'+$table+' tbody').prepend($(data).addClass('success'));
				}
				else if ($method == 'update' || $method == 'update_lic') {
					$('#row-'+$.url($action).param('id')).replaceWith($(data).addClass('success'));
				}
				else if ($method == 'notes') {
					$('[data-team="'+$.url($action).param('id')+'"]').replaceWith($(data));
				}
				
				$('.modal').modal('hide');
			}
		});
	});
$(document).on("click",'.thirty', function(){
	$('.show_thirty').toggle();
});
$(document).on("click",'.ninety', function(){
	var table = $('.show_ninety table tr td').text();
	if(table !== ""){
		$('.show_ninety').toggle();
	}
});
$(document).on("click",'.licenses', function(e){
	if ($(this).is(':checked')) {
		var license = $(this).val();
		$('.cheched_license').val(license);
		$('.show_date').show();
	}else{
		e.preventDefault();
		if (confirm($(this).data('confirm')) == true) {
	    	var qlf = $(this).val();
	    	var empl = $(this).data('empl');
	    	var _this = $(this);
			$.ajax({
				method:"POST",
				url: 'ajax.php?table=qualification&method=modal&action=delete',
				data: { qlf_id : qlf,empl_id : empl},
				success: function(data){
					var result = JSON.parse(data);
					_this.prop('checked', false);
				}
			});
	    }
	}
});
$(document).on("focus",'.popup_date ', function(){
	$('.show_date').css('height','365px');
	$('.save_license').css('margin-top','260px');
})
$(document).on("blur",'.popup_date ', function(){
	$('.show_date').css('height','auto');
	$('.save_license').css('margin-top','0px');
})
$(document).on("click",'.save_license', function(){
	var date = $('.popup_date').val();
	var empl_id = $('.empl_id').val();
	var license = $('.cheched_license').val();
	if(date.length != ""){
		$.ajax({
			method:"POST",
			url: 'ajax.php?table=qualification&method=modal&action=update',
			data: {date:date,emp_id:empl_id,checkbox:license},
			success: function(data){
				$('.show_date').hide();	
				var obj = jQuery.parseJSON(data);
				var added_count = 0,
					qu_count = 0,
					qu_count_updates = 0,
					emp_id = $('.modal').data('emp-id'),
					tr = $('a[data-emp-id="'+emp_id+'"]').parents('tr');

				qu_count = obj['qu_count'];
				qu_count_updates = obj['qu_count_updates'];
				days_count = obj['days_count'];

				added_count = tr.children('td:nth-child(5)').html();
				tr.children('td:nth-child(5)').html(added_count);
				tr.children('td:nth-child(5)').html(added_count);
				if(days_count>0 && days_count<30){
					added_count = parseInt(tr.children('td:nth-child(2)').html())+qu_count;
					tr.children('td:nth-child(2)').html(added_count);
					added_count = parseInt(tr.children('td:nth-child(3)').html())+qu_count;
					tr.children('td:nth-child(3)').html(added_count);
				}
				else if(days_count>30 && days_count<90){
					added_count = parseInt(tr.children('td:nth-child(2)').html())+qu_count;
					tr.children('td:nth-child(2)').html(added_count);
					added_count = parseInt(tr.children('td:nth-child(4)').html())+qu_count;
					tr.children('td:nth-child(4)').html(added_count);
				}
				
			}
	    });
	}else{
		$('.error_msg').text('Date is required');
	}
});
$(document).on("click", ".popup_save_btn",function(){

	var form = new FormData($('#modal_form_id')[0]);
	var _this = $(this);
    $.ajax({
		method:"POST",
		url: 'ajax.php?table=qualification&method=modal&action=upload',
		data: form,
		contentType: false,
		processData: false,
		success: function(data){
			var obj = jQuery.parseJSON(data);
			var added_count = 0,
				arr = 0,
				qu_count = 0,
				qu_count_updates = 0,
				emp_id = $('.modal').data('emp-id'),
				tr = $('a[data-emp-id="'+emp_id+'"]').parents('tr');

			qu_count = obj['qu_count'];
			qu_count_updates = obj['qu_count_updates'];
			days_count = obj['days_count'];

			if(typeof obj['files'] != 'undefined'){
				arr = obj['files'].length;
			};
			// $("#mytable4 td").unbind();
			$(".close").trigger("click");
			$(".span_close").unbind('click');
			added_count = tr.children('td:nth-child(5)').html();
			added_count = parseInt(added_count)+arr;
			tr.children('td:nth-child(5)').html(added_count);
			tr.children('td:nth-child(5)').html(added_count);
			if(days_count>0 && days_count<30){
				added_count = parseInt(tr.children('td:nth-child(2)').html())+qu_count;
				tr.children('td:nth-child(2)').html(added_count);
				added_count = parseInt(tr.children('td:nth-child(3)').html())+qu_count;
				tr.children('td:nth-child(3)').html(added_count);
			}
			else if(days_count>30 && days_count<90){
				added_count = parseInt(tr.children('td:nth-child(2)').html())+qu_count;
				tr.children('td:nth-child(2)').html(added_count);
				added_count = parseInt(tr.children('td:nth-child(4)').html())+qu_count;
				tr.children('td:nth-child(4)').html(added_count);
			}
			if($(_this).hasClass('save_and_next')){
				$(tr).next().find('.modal-link').click();
			}
		}
    });

});

	// change user level, show perms if 'User'
	$(document).on('change', 'select[name="level"]', function () {
		if ($(this).val() == 'User') {
			$('#permissions').addClass('in');
		}
		else {
			$('#permissions').removeClass('in');
		}
	});
	
	// datepicker for teams page
	$('.datepicker').datepicker({
		autoclose: true,
		orientation: 'bottom right'		
	}).on('changeDate', function (e) {
		// team date changed, redirect to teams.php?date=
		window.location.href='teams.php?date='+$(this).val();
	});
	
	
	// options for droppable teams, called more than once below
	$droppable_options = {
		hoverClass: 'list-group-item-success',
		accept: '[data-employee], [data-vehicle]',
		drop: function (e, ui) {
			$team = $(this).data('team');
			$location = $(this).closest('[data-location]').data('location');
			$employees = parseInt($(this).data('employees'));
			$vehicles = parseInt($(this).data('vehicles'));
			$ajax = null;
			
			if (ui.draggable.data('employee')) {
				if ($employees < 5) {
					$ajax = 'ajax.php?table=team_employees&method=create&id_team='+$team+'&id_location='+$location+'&id_employee='+ui.draggable.data('employee');
					$remove = $('[data-employee="'+ui.draggable.data('employee')+'"]');
				}
			}
			else if (ui.draggable.data('vehicle')) {
				if ($vehicles < 5) {
					$ajax = 'ajax.php?table=team_vehicles&method=create&id_team='+$team+'&id_location='+$location+'&id_vehicle='+ui.draggable.data('vehicle');
					$remove = $('[data-vehicle="'+ui.draggable.data('vehicle')+'"]');
				}
			}
			
			if ($ajax) {
				$.get($ajax, function (data) {
					if (data == 'no permission') {
						no_permission_alert();
					}
					else {
						$('[data-team="'+$team+'"]').replaceWith(data);
						$remove.remove();						
						update_tabs($droppable_options);
					}
				});
			}
			else {
				alert('A team can only have up to 5 employees and 5 vehicles.');
			}
		}
	}
	
	// delete row/team
	$(document).on('click', '.delete-link', function (e) {
		e.preventDefault();
		
		if (confirm($(this).data('confirm')) == true) {
			$href = $(this).attr('href');
			$table = $.url($href).param('table');
			$id = $.url($href).param('id');
			
			if ($table == 'teams') {
				$selector = $('[data-team="'+$id+'"]');
			}
			else {
				$selector = $('#row-'+$id);
			}
			
			$.get($href, function (data) {
				if (data == 'no permission') {
					no_permission_alert();
				}
				else {
					$selector.remove();					
					update_tabs($droppable_options);
				}
			});
		} 
    });
	
	// delete team employee/vehicle
	$(document).on('click', '.delete-team-player-link', function (e) {
		e.preventDefault();
	
		$href = $(this).attr('href');
		$table = $.url($href).param('table');
		$id = $.url($href).param('id');
		$id_team = $.url($href).param('id_team');
		
		if ($table == 'team_employees') {
			$selector = $('[data-team-employee="'+$id+'"]');
		}
		else if ($table == 'team_vehicles') {
			$selector = $('[data-team-vehicle="'+$id+'"]');
		}
		
		$.get($href, function (data) {
			if (data == 'no permission') {
				no_permission_alert();
			}
			else {
				$selector.remove();			
				$('[data-team="'+$id_team+'"]').replaceWith(data);				
				update_tabs($droppable_options);
			}
		});
    });
	
	// copy location previous date teams
	$(document).on('click', '.copy-team-link', function (e) {
		e.preventDefault();
		
		if (confirm($(this).data('confirm')) == true) {
			$href = $(this).attr('href');
			$id_location = $.url($href).param('id_location');
			$date = $.url($href).param('date');
			
			$.get($href, function (data) {
				if (data == 'no permission') {
					no_permission_alert();
				}
				else {
					$('[data-location="'+$id_location+'"]').replaceWith(data);	
					update_tabs($droppable_options);
				}
			});
		}
    });
	
	// create team
	$(document).on('click', '.create-team-link', function (e) {
		e.preventDefault();
		
		$href = $(this).attr('href');
		$id_location = $.url($href).param('id_location');
		
		$.get($href, function (data) {
			if (data == 'no permission') {
				no_permission_alert();
			}
			else {
				$team = $(data);
				$('[data-location="'+$id_location+'"] .list-group').append($team);		
				drag_drop($droppable_options);
			}
		});
    });
	
    drag_drop($droppable_options);
	
	$('[data-toggle="popover"]').popover({
		html: true,
		placement: 'left'
	});
});

function no_permission_alert () {
	alert('You do not have sufficient permission to perform this action.');
}

function update_tabs (droppable_options) {
	if ($('#team-tabs').length) {
		$active_tab = $('#team-tabs .tab-pane.active').attr('id');
		$timestamp = $('#team-tabs').data('timestamp');
		
		$.get('ajax.php?table=teams&method=tabs&active_tab='+$active_tab+'&timestamp='+$timestamp, function (data) {
			$('#team-tabs').replaceWith(data);
			drag_drop(droppable_options);
		});
	}
}

function drag_drop (droppable_options) {
	// draggable employees & vehicles
    $('[data-employee], [data-vehicle]').draggable({
		helper: 'clone'
    });
	
	// disable draggable button clicking
	$(document).on('click', '[data-employee], [data-vehicle]', function (e) {
		e.preventDefault();
    });	
	
	// droppable teams
    $('[data-team]').droppable(droppable_options);	
}