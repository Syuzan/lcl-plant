<?php

class LicensesOutput {
	public $db;
	public $user;
	public $main_sql = "select * from `qualifications`";
	public $rows;
	public $row;

	function __construct ($db, $user) {
		$this->db = $db;
		$this->user = $user;
	}

	function validateDate($date, $format = 'Y-m-d H:i:s')
	{
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	function fetchRows () {
		
		$sql = $this->main_sql."order by `name` asc";
		$stm = $this->db->prepare($sql);
		$stm->execute();
		$res = $stm->fetchAll();

		if ($res) {
			array_walk_recursive($res, 'sanitize');
			$this->rows = $res;
		}
	}


	function fetchRow ($id) {
		$sql = $this->main_sql."where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $id);
		$stm->execute();
		$res = $stm->fetch();
		array_walk_recursive($res, 'sanitize');		
		$this->row = $res;
	}

	function showTable () {
		?>
		<div class="table-responsive">
			<table id="table-qualification" class="table table-hover">
				<thead>
					<tr>
						<th>Name</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php $this->showRows(); ?>
				</tbody>
			</table>
		</div>
		<?php
	}

	function showRows () {
		if (empty($this->rows)) {
			?>
			<tr id="row-empty">
				<td colspan="100">No Licenses to display.</td>
			</tr>
			<?php
		}
		else {
			foreach ($this->rows as $this->row) {
				$this->showRow();
			}
		}
	}

	function showRow () {
		?>
		<tr id="row-<?php echo $this->row['id']; ?>">
			<td><?php echo $this->row['name']; ?></td>
			<td class="text-right nowrap">
				<a href="ajax.php?table=qualification&method=modal&action=update_lic&id=<?php echo $this->row['id']; ?>" title="Update" class="btn btn-primary modal-link"><i class="fa fa-pencil"></i></a>
				<a href="ajax.php?table=qualification&method=delete_lic&id=<?php echo $this->row['id']; ?>" data-confirm="Are you sure you want to delete <?php echo $this->row['name']; ?>?" title="Delete" class="btn btn-danger delete-link"><i class="fa fa-trash"></i></a>
			</td>
		</tr>
		<?php
	}
	
	function showTags () {
		foreach ($this->rows as $this->row) {
			?>
			<a href="#" class="btn btn-primary btn-sm" data-employee="<?php echo $this->row['id']; ?>"><i class="fa fa-user"></i> <?php echo $this->row['name']; ?></a>
			<?php
		}
	}
	
	function createLicense () {
		?>
		<div class="modal" tabindex="-1" role="dialog" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Create License</h4>
					</div>
					<div class="modal-body">
						<form method="post" action="ajax.php?table=qualification&method=create_lic" class="form-horizontal modal-form">
							<?php
							$fo = new FormOutput($this->db, $this->user);
							$fo->showInput('text', 'Name', 'name', '', true);
							$fo->showSubmit('Create');
							?>
						</form>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	function modalUpdate () {
		?>
		<div class="modal" tabindex="-1" role="dialog" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Update License</h4>
					</div>
					<div class="modal-body">
						<form method="post" action="ajax.php?table=qualification&method=update_lic&id=<?php echo $this->row['id']; ?>" class="form-horizontal modal-form">
							<?php
							$fo = new FormOutput($this->db, $this->user);
							$fo->showInput('text', 'Name', 'name', $this->row['name'], true);
							$fo->showSubmit('Update');
							?>
						</form>
					</div>
				</div>
			</div>
		</div>
		<?php
	}

}

?>