<?php

class FormOutput {
	public $db;
	public $user;
	
	function __construct ($db = null, $user = null) {
		$this->db = $db;
		$this->user = $user;
	}
	
	function showInput ($type, $label, $name, $value = '', $required = false) {
		?>
		<div class="form-group">
			<label class="col-sm-3 control-label"><?php echo $label; ?></label>
			<div class="col-sm-6">
				<input name="<?php echo $name; ?>" type="<?php echo $type; ?>" value="<?php echo $value; ?>" class="form-control"<?php if ($required) echo ' required'; ?>>
			</div>
		</div>
		<?php
	}
	
	function showSelect ($label, $name, $options, $selected = '', $required = false) {
		?>
		<div class="form-group">
			<label class="col-sm-3 control-label"><?php echo $label; ?></label>
			<div class="col-sm-6">
				<select name="<?php echo $name; ?>" class="form-control"<?php if ($required) echo ' required'; ?>>
					<option value=""></option>
					<?php
					foreach ($options as $k => $v) {
						echo '<option value="'.$k.'"'.(($k == $selected) ? ' selected' : '').'>'.$v.'</option>';
					}
					?>
				</select>
			</div>
		</div>
		<?php
	}
	
	function showTextarea ($label, $name, $value = '', $required = false) {
		?>
		<div class="form-group">
			<label class="col-sm-3 control-label"><?php echo $label; ?></label>
			<div class="col-sm-6">
				<textarea name="<?php echo $name; ?>" rows="5" class="form-control"<?php if ($required) echo ' required'; ?>><?php echo $value; ?></textarea>
			</div>
		</div>
		<?php
	}
	
	function showHidden ($name, $value) {
		?>
		<input name="<?php echo $name; ?>" type="hidden" value="<?php echo $value; ?>">
		<?php
	}
	
	function showPermissions ($user_level = null, $selected = '') {
		if (empty($selected)) {
			$selected = array();
		}
		else {
			$selected = explode('|', $selected);
		}
		?>
		<div id="permissions" class="form-group collapse<?php if ($user_level == 'User') echo ' in'; ?>">
			<label class="col-sm-3 control-label">Permissions</label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?php
					$li = new Listing();
					$permissions = $li->userPermissions();					
					$lo = new LocationOutput($this->db, $this->user);
					$lo->fetchRows();
					$location_count = count($lo->rows);
					$count = 1;
					
					foreach ($lo->rows as $location) {
						echo $location['name'].'<br>';
						foreach ($permissions as $p) {
							$p_id = $p.'_'.$location['id'];
							$p_nice = ucwords(str_replace('_', ' ', $p));
							echo '<div class="checkbox"><label><input type="checkbox" name="permissions['.$p_id.']"'.((in_array($p_id, $selected)) ? ' checked' : '').'>'.$p_nice.'</label></div>';
						}
						if ($count < $location_count) {
							echo '<br><br>';
						}
						$count++;
					}
					?>
				</div>
			</div>
		</div>
		<?php
	}

	function showSubmit ($label) {
		?>
		<div class="form-group text-center"> 
			<button type="submit" class="btn btn-primary"><?php echo $label; ?></button>
		</div>
		<?php
	}
}

?>