<?php

class AuditOutput {
	public $db;
	public $user;
	public $main_sql = "select a.*, u.username
						from `audits` as a
						left join `users` as u on (u.id = a.id_user)";
	public $rows;
	public $row;

	function __construct ($db, $user) {
		$this->db = $db;
		$this->user = $user;
	}

	function fetchRows () {
		$sql = $this->main_sql." order by a.date desc limit 100";
		$stm = $this->db->prepare($sql);
		$stm->execute();
		$res = $stm->fetchAll();

		if ($res) {
			array_walk_recursive($res, 'sanitize');			
			$this->rows = $res;
		}
	}

	function showTable () {
		?>
		<div class="table-responsive">
			<table id="table-audits" class="table table-hover">
				<thead>
					<tr>
						<th>Date</th>
						<th>User</th>
						<th>Message</th>
						<th>Before</th>
						<th>After</th>
					</tr>
				</thead>
				<tbody>
					<?php $this->showRows(); ?>
				</tbody>
			</table>
		</div>
		<?php
	}

	function showRows () {
		if (empty($this->rows)) {
			?>
			<tr id="row-empty">
				<td colspan="100">No audits to display.</td>
			</tr>
			<?php
		}
		else {
			foreach ($this->rows as $this->row) {
				$this->showRow();
			}
		}
	}

	function showRow () {
		?>
		<tr id="row-<?php echo $this->row['id']; ?>">
			<td><?php echo date('d/m/Y h:m A', $this->row['date']); ?></td>
			<td><?php echo $this->row['username']; ?></td>
			<td><?php echo $this->row['message']; ?></td>
			<td>
				<?php
				$this->row['before'] = json_decode(htmlspecialchars_decode($this->row['before'], ENT_QUOTES), true);
				if (!empty($this->row['before'])) {
					echo '<button type="button" data-toggle="popover" data-content="<pre>'.print_r($this->row['before'], true).'</pre>" class="btn btn-primary"><i class="fa fa-file-text"></i></button>';
				}
				?>				
			</td>
			<td>
				<?php
				$this->row['after'] = json_decode(htmlspecialchars_decode($this->row['after'], ENT_QUOTES), true);
				if (!empty($this->row['after'])) {
					echo '<button type="button" data-toggle="popover" data-content="<pre>'.print_r($this->row['after'], true).'</pre>" class="btn btn-primary"><i class="fa fa-file-text"></i></button>';
				}
				?>				
			</td>
		</tr>
		<?php
	}
}

?>