<?php

class Audit {
	public $db;
	public $user;
	public $id_user;
	public $message;
	public $before;
	public $after;
	public $date;

	function __construct ($db, $user) {
		$this->db = $db;
		$this->user = $user;
	}

	function create () {
		if (!empty($this->before) || !empty($this->after)) {
			$this->before = json_encode($this->before);
			$this->after = json_encode($this->after);
		}
		
		$sql = "insert into `audits` (`id_user`, `message`, `before`, `after`, `date`) values (:id_user, :message, :before, :after, unix_timestamp())";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_user', $this->user->id);
		$stm->bindParam(':message', $this->message);
		$stm->bindParam(':before', $this->before);
		$stm->bindParam(':after', $this->after);
		$stm->execute();
	}
}

?>