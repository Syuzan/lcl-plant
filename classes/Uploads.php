<?php

class Uploads {
	public $db;
	public $user;
	public $id;
	public $name;
	public $directory;
	public $id_employee;

	function __construct ($db, $user) {
		$this->db = $db;
		$this->user = $user;
	}

	function populateByPost () {
		array_walk_recursive($_POST, 'desanitize');
		
		foreach ($_POST as $k => $v) {
			if (property_exists($this, $k)) {
				$this->$k = $v;
			}
		}
	}

	function populateById () {
		$sql = "select * from `employees` where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
		$res = $stm->fetch();

		if ($res) {
			array_walk_recursive($res, 'sanitize');
			
			foreach ($res as $k => $v) {
				$this->$k = $v;
			}
		}
	}

	function uploads_count($id_employee){
		$cq = $this->select_by_id($id_employee);
			$uploads_count = 0;
		foreach ($cq as $key => $value) {
			if(!empty($value["name"])){
				$uploads_count++;
			}
			else{
				$uploads_count = 0;
			}
			
		}
		return $uploads_count;

	}

	function select_by_id ($emp_id) {
		$sql = "select * from uploads where id_employee = :emp_id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':emp_id', $emp_id);
		$stm->execute();
		$res = $stm->fetchAll();
		return $res;
	}

	function select_by_file_id ($id) {
		$sql = "select * from uploads where id = :id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $id);
		$stm->execute();
		$res = $stm->fetch();
		return $res;
	}

	function create ($name, $directory, $id_employee, $file_name) {
		$sql = "insert into `uploads` (`name`, directory, id_employee, file_name) 
				values (:name, :directory, :id_employee, :file_name)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':name', $name);
		$stm->bindParam(':directory', $directory);
		$stm->bindParam(':id_employee', $id_employee);
		$stm->bindParam(':file_name', $file_name);
		$stm->execute();

		$this->id = $this->db->lastInsertId();
	}

	function update () {
		$sql = "update `employees` set `name`=:name where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':name', $this->name);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
	}

	function delete ($id) {
		$sql = "DELETE FROM `uploads` where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $id);
		$stm->execute();
	}
}

?>