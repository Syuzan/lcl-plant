<?php

class Location {
	public $db;
	public $user;
	public $id;
	public $name;

	function __construct ($db, $user) {
		$this->db = $db;
		$this->user = $user;
	}

	function populateByPost () {
		array_walk_recursive($_POST, 'desanitize');
		
		foreach ($_POST as $k => $v) {
			if (property_exists($this, $k)) {
				$this->$k = $v;
			}
		}
	}

	function populateById () {
		$sql = "select * from `locations` where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
		$res = $stm->fetch();

		if ($res) {
			array_walk_recursive($res, 'sanitize');
			
			foreach ($res as $k => $v) {
				$this->$k = $v;
			}
		}
	}

	function create () {
		$sql = "insert into `locations` (`name`) values (:name)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':name', $this->name);
		$stm->execute();

		$this->id = $this->db->lastInsertId();
	}

	function update () {
		$sql = "update `locations` set `name`=:name where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':name', $this->name);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
	}

	function delete () {
		$sql = "update `locations` set `deleted`=1 where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
	}
}

?>