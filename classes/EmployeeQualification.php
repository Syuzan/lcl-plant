<?php

class EmployeeQualification {
	public $db;
	public $user;
	public $id;
	public $id_employee;
	public $id_qualification;
	public $expiration_date;

	function __construct ($db, $user, $id_employee, $id_qualification, $expiration_date) {
		$this->db = $db;
		$this->user = $user;
		$this->id_employee = $id_employee;
		$this->id_qualification = $id_qualification;
		$this->expiration_date = $expiration_date;
	}

	function populateByPost () {
		array_walk_recursive($_POST, 'desanitize');
		
		foreach ($_POST as $k => $v) {
			if (property_exists($this, $k)) {
				$this->$k = $v;
			}
		}
	}

	function populateById () {
		$sql = "select * from `employees` where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
		$res = $stm->fetch();

		if ($res) {
			array_walk_recursive($res, 'sanitize');
			
			foreach ($res as $k => $v) {
				$this->$k = $v;
			}
		}
	}



	function check_qualif_exists(){
		$sql = "select * from employee_qualification where id_employee = :id_employee AND id_qualification = :id_qualification";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_employee', $this->id_employee);
		$stm->bindParam(':id_qualification', $this->id_qualification);
		$stm->execute();
		$res = $stm->fetch();
		return $res;		
	}

	function create () {
		$sql = "insert into `employee_qualification` (`id_employee`, id_qualification, expiration_date) 
				values (:id_employee, :id_qualification, :expiration_date)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_employee', $this->id_employee);
		$stm->bindParam(':id_qualification', $this->id_qualification);
		$stm->bindParam(':expiration_date', $this->expiration_date);
		$stm->execute();

		$this->id = $this->db->lastInsertId();
	}

	function update () {
		$sql = "UPDATE employee_qualification SET expiration_date = :expiration_date 
				WHERE id_employee = :id_employee AND id_qualification = :id_qualification";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_employee', $this->id_employee);
		$stm->bindParam(':id_qualification', $this->id_qualification);
		$stm->bindParam(':expiration_date', $this->expiration_date);
		$stm->execute();
	}

	function delete () {
		$sql = "update `employees` set `deleted`=1 where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
	}
	function delete_qlf () {
		$sql = "DELETE FROM `employee_qualification` where `id_employee`=:id_employee AND `id_qualification`=:id_qualification";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_employee', $this->id_employee);
		$stm->bindParam(':id_qualification', $this->id_qualification);
		$stm->execute();
	}
}

?>