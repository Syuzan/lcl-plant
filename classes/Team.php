<?php

class Team {
	public $db;
	public $user;
	public $id;
	public $id_display;
	public $id_location;
	public $date;
	public $notes;

	function __construct ($db, $user) {
		$this->db = $db;
		$this->user = $user;
	}

	function populateByPost () {
		array_walk_recursive($_POST, 'desanitize');
		
		foreach ($_POST as $k => $v) {
			if (property_exists($this, $k)) {
				$this->$k = $v;
			}
		}
	}

	function populateById () {
		$sql = "select * from `teams` where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
		$res = $stm->fetch();

		if ($res) {
			array_walk_recursive($res, 'sanitize');
			
			foreach ($res as $k => $v) {
				$this->$k = $v;
			}
		}
	}

	function create () {
		$sql = "select count(*) from `teams` where `id_location`=:id_location and `date`=:date";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_location', $this->id_location);
		$stm->bindParam(':date', $this->date);
		$stm->execute();
		$count = $stm->fetchColumn();
		
		$this->id_display = $count + 1;
		
		$sql = "insert into `teams` (`id_display`, `id_location`, `date`) values (:id_display, :id_location, :date)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_display', $this->id_display);
		$stm->bindParam(':id_location', $this->id_location);
		$stm->bindParam(':date', $this->date);
		$stm->execute();

		$this->id = $this->db->lastInsertId();
	}

	function copy () {
		$previous_date = strtotime('-1 day', $this->date);
		
		// select teams from previous date
		$sql = "select * from `teams` where `id_location`=:id_location and `date`=:previous_date";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_location', $this->id_location);
		$stm->bindParam(':previous_date', $previous_date);
		$stm->execute();
		$teams = $stm->fetchAll();

		// insert team into date
		foreach ($teams as $team) {
			$this->create();		
			$new_id_team = $this->db->lastInsertId();
			
			// insert team employees
			$sql = "select * from `team_employees` where `id_team`=:id_team";
			$stm = $this->db->prepare($sql);
			$stm->bindParam(':id_team', $team['id']);
			$stm->execute();
			$team_employees = $stm->fetchAll();
			
			foreach ($team_employees as $team_employee) {
				$te = new TeamEmployee($this->db, $this->user);
				$te->id_team = $new_id_team;
				$te->id_employee = $team_employee['id_employee'];
				$te->create();	
			}
			
			// insert team vehicles
			$sql = "select * from `team_vehicles` where `id_team`=:id_team";
			$stm = $this->db->prepare($sql);
			$stm->bindParam(':id_team', $team['id']);
			$stm->execute();
			$team_vehicles = $stm->fetchAll();
			
			foreach ($team_vehicles as $team_vehicle) {
				$tv = new TeamVehicle($this->db, $this->user);
				$tv->id_team = $new_id_team;
				$tv->id_vehicle = $team_vehicle['id_vehicle'];
				$tv->create();
			}
		}
	}

	function updateNotes () {
		$sql = "update `teams` set `notes`=:notes where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':notes', $this->notes);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
	}

	function delete () {
		$sql = "delete from `teams` where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
	}
}

?>