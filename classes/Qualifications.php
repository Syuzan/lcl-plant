<?php

class Qualifications {
	public $db;
	public $user;
	public $id;
	public $name;

	function __construct ($db, $user) {
		$this->db = $db;
		$this->user = $user;
	}

	function populateByPost () {
		array_walk_recursive($_POST, 'desanitize');
		
		foreach ($_POST as $k => $v) {
			if (property_exists($this, $k)) {
				$this->$k = $v;
			}
		}
	}

	function populateById () {
		$sql = "select * from `qualifications` where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
		$res = $stm->fetch();

		if ($res) {
			array_walk_recursive($res, 'sanitize');
			
			foreach ($res as $k => $v) {
				$this->$k = $v;
			}
		}
	}

	function select () {
		$sql = "select * from `qualifications`";
		$stm = $this->db->prepare($sql);
		// $stm->bindParam(':name', $this->name);
		$stm->execute();
		// $result = $stm->setFetchMode(PDO::FETCH_ASSOC);
		$result = $stm->fetchAll();
		return $result;
		$this->id = $this->db->lastInsertId();
	}
	function create_license ($name = "") {
		if(!empty($name)){
			$sql = "insert into `qualifications` (`name`) values (:name)";
			$stm = $this->db->prepare($sql);
			$stm->bindParam(':name', $name);
			$stm->execute();
			$this->id = $this->db->lastInsertId();
		}	
	}
	function create () {
		$sql = "insert into `qualifications` (`name`) values (:name)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':name', $this->name);
		$stm->execute();

		$this->id = $this->db->lastInsertId();
	}

	function update () {
		$sql = "update `qualifications` set `name`=:name where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':name', $this->name);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
	}

	function delete () {
		$sql = "DELETE FROM qualifications WHERE id =  :id";
		// $sql = "update `qualifications` set `deleted`=1 where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
	}
}

?>