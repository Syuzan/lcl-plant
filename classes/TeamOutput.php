<?php

class TeamOutput {
	public $db;
	public $user;
	public $date;
	public $timestamp;
	public $locations = array();
	public $location = array();
	public $teams = array();
	public $exclude_teams;
	public $team = array();

	function __construct ($db, $user, $print = false) {
		$this->db = $db;
		$this->user = $user;
		$this->print = $print;
	}
	
	function setDateFields ($date, $timestamp) {
		$this->date = $date;
		$this->timestamp = $timestamp;
	}
	
	function fetchLocations () {
		$sql = "select * from `locations` where `deleted`=0 order by `name` asc";
		$stm = $this->db->prepare($sql);
		$stm->execute();
		$res = $stm->fetchAll();

		if ($res) {
			array_walk_recursive($res, 'sanitize');			
			$this->locations = $res;
		}
	}
	
	function fetchLocation ($id) {
		$sql = "select * from `locations` where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $id);
		$stm->execute();
		$res = $stm->fetch();

		if ($res) {
			array_walk_recursive($res, 'sanitize');			
			$this->location = $res;
		}
	}
	
	function fetchLocationTeams ($id_location) {
		$sql = "select t.*, (select count(*) from `team_employees` where `id_team`=t.id) as employees, (select count(*) from `team_vehicles` where `id_team`=t.id) as vehicles
				from `teams` as t
				where t.id_location=:id_location and date(from_unixtime(t.date))=date(from_unixtime(:timestamp)) order by t.date asc";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_location', $id_location);
		$stm->bindParam(':timestamp', $this->timestamp);
		$stm->execute();
		$res = $stm->fetchAll();

		array_walk_recursive($res, 'sanitize');			
		$this->teams = $res;
	}
	
	function fetchExcludeTeams () {
		$sql = "select * from `teams` where date(from_unixtime(`date`))=date(from_unixtime(:timestamp))";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':timestamp', $this->timestamp);
		$stm->execute();
		$res = $stm->fetchAll();
		$exclude_teams = array();
		
		foreach ($res as $r) {
			$exclude_teams[] = $r['id'];
		}
		
		$this->exclude_teams = implode(',', $exclude_teams);
	}
	
	function fetchTeam ($id) {
		$sql = "select t.*, (select count(*) from `team_employees` where `id_team`=t.id) as employees, 
				(select count(*) from `team_vehicles` where `id_team`=t.id) as vehicles
				from `teams` as t
				where t.id=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $id);
		$stm->execute();
		$res = $stm->fetch();

		array_walk_recursive($res, 'sanitize');		
		$this->team = $res;
	}

	function showLocations () {	
		?>
		<div class="row" id="team-locations">
			<?php
			foreach ($this->locations as $this->location) {
				$this->showLocation();
			}
			?>		
		</div>
		<?php
	}
	
	function showLocation () {
		?>
		<?php if ($this->user->level == 'Admin' || in_array('view_teams_'.$this->location['id'], $this->user->permissions)) { ?>
			<div data-location="<?php echo $this->location['id']; ?>" class="col-md-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						<?php if (($this->user->level == 'Admin' || in_array('create_teams_'.$this->location['id'], $this->user->permissions)) && !$this->print) { ?>
							<div class="pull-right">
								<a href="ajax.php?table=teams&method=copy&id_location=<?php echo $this->location['id']; ?>&date=<?php echo $this->timestamp; ?>" data-confirm="Are you sure you want to copy all teams from the previous date into this location?" title="Copy Previous Day" class="btn btn-primary btn-sm copy-team-link"><i class="fa fa-copy fa-fw"></i></a>
								<a href="ajax.php?table=teams&method=create&id_location=<?php echo $this->location['id']; ?>&date=<?php echo $this->timestamp; ?>" title="Create Team" class="btn btn-primary btn-sm create-team-link"><i class="fa fa-plus fa-fw"></i></a>
							</div>
						<?php } ?>
						<?php echo $this->location['name']; ?>
					</div>
					<ul class="list-group">
						<?php
						$this->fetchLocationTeams($this->location['id']);							
						foreach ($this->teams as $this->team) {
							$this->showTeam();
						}
						?>
					</ul>
				</div>
			</div>
		<?php } ?>
		<?php
	}
	
	function showTeam () {
		?>
		<li data-team="<?php echo $this->team['id']; ?>" data-employees="<?php echo $this->team['employees']; ?>" data-vehicles="<?php echo $this->team['vehicles']; ?>" class="list-group-item">
			<?php if (!$this->print) { ?>
				<div class="pull-right">
					<?php if ($this->user->level == 'Admin' || in_array('update_team_notes_'.$this->team['id_location'], $this->user->permissions)) { ?>
						<a href="ajax.php?table=teams&method=modal&action=notes&id=<?php echo $this->team['id']; ?>&id_location=<?php echo $this->team['id_location']; ?>" title="Notes" class="btn btn-<?php if (empty($this->team['notes'])) echo 'primary'; else echo 'info'; ?> btn-sm modal-link"><i class="fa fa-file-text fa-fw"></i></a>
					<?php } ?>				
					<?php if ($this->user->level == 'Admin' || in_array('delete_teams_'.$this->team['id_location'], $this->user->permissions)) { ?>
						<a href="ajax.php?table=teams&method=delete&id=<?php echo $this->team['id']; ?>&id_location=<?php echo $this->team['id_location']; ?>" data-confirm="Are you sure you want to delete team #<?php echo $this->team['id_display']; ?>?" title="Delete Team" class="btn btn-danger btn-sm delete-link"><i class="fa fa-trash fa-fw"></i></a>
					<?php } ?>
				</div>
			<?php } ?>
			<h5>Team #<?php echo $this->team['id_display']; ?></h5>	
			<div class="clearfix">
				<?php
				$teo = new TeamEmployeeOutput($this->db, $this->user, $this->team['id'], $this->print);
				$teo->fetchRows();
				$teo->showTags();
				?>
				<br>
				<?php
				$tvo = new TeamVehicleOutput($this->db, $this->user, $this->team['id'], $this->print);
				$tvo->fetchRows();
				$tvo->showTags();
				?>
			</div>
		</li>
		<?php
	}
	
	function showTabs ($active_tab = null) {
		?>
		<div data-timestamp="<?php echo $this->timestamp; ?>" id="team-tabs" class="clearfix">
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation"<?php if ($active_tab == null || $active_tab == 'employees') echo ' class="active"'; ?>><a href="#employees" aria-controls="employees" role="tab" data-toggle="tab">Employees</a></li>
				<li role="presentation"<?php if ($active_tab == 'vehicles') echo ' class="active"'; ?>><a href="#vehicles" aria-controls="vehicles" role="tab" data-toggle="tab">Vehicles</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane<?php if ($active_tab == null || $active_tab == 'employees') echo ' active'; ?>" id="employees">
					<?php
					$this->fetchExcludeTeams();					
					$eo = new EmployeeOutput($this->db, $this->user);
					$eo->fetchRows($this->exclude_teams);
					$eo->showTags();
					?>
				</div>
				<div role="tabpanel" class="tab-pane<?php if ($active_tab == 'vehicles') echo ' active'; ?>" id="vehicles">
					<?php
					$vo = new VehicleOutput($this->db, $this->user);
					$vo->fetchRows($this->exclude_teams);
					$vo->showTags();
					?>
				</div>
			</div>
		</div>
		<?php
	}
	
	function modalNotes () {
		?>
		<div class="modal" tabindex="-1" role="dialog" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Team #<?php echo $this->team['id_display']; ?> Notes</h4>
					</div>
					<div class="modal-body">
						<form method="post" action="ajax.php?table=teams&method=notes&id=<?php echo $this->team['id']; ?>" class="form-horizontal modal-form">
							<?php
							$fo = new FormOutput($this->db, $this->user);
							$fo->showTextarea('Notes', 'notes', $this->team['notes']);
							$fo->showHidden('id_location', $this->team['id_location']);
							$fo->showSubmit('Update');
							?>
						</form>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
}

?>