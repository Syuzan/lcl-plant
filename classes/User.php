<?php

class User {
	public $db;
	public $id;
	public $username;
	public $password;
	public $secret;
	public $level;
	public $permissions = array();
	public $is_signed_in = false;

	function __construct ($db) {
		$this->db = $db;
	}
	
	function populateByPost () {
		array_walk_recursive($_POST, 'desanitize');
		
		foreach ($_POST as $k => $v) {
			if (property_exists($this, $k)) {
				$this->$k = $v;
			}
		}
		
		$permissions = array();
		foreach ($this->permissions as $k => $v) {
			$permissions[] = $k;
		}
		$this->permissions = $permissions;
	}

	function populateById () {
		$sql = "select * from `users` where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
		$res = $stm->fetch();

		if ($res) {
			array_walk_recursive($res, 'sanitize');
			
			foreach ($res as $k => $v) {
				$this->$k = $v;
			}
			
			$this->permissions = explode('|', $this->permissions);
		}
	}
	
	function credentialsValid () {
		$sql = "select * from `users` where `deleted`=0 and `username`=:username";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':username', $this->username);
		$stm->execute();
		$res = $stm->fetch();

		if ($res) {
			if (password_verify($this->password, $res['password'])) {
				$this->id = $res['id'];
				$this->secret = password_hash($this->password.$_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'], PASSWORD_DEFAULT);
				
				return true;
			}
		}
		
		return false;
	}

	function signIn () {				
		$_SESSION['id'] = $this->id;
		$_SESSION['secret'] = $this->secret;

		$sql = "update `users` set `secret`=:secret where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':secret', $this->secret);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
	}

	function setIsSignedIn () {
		$sql = "select * from `users` where `deleted`=0 and `id`=:id and `secret`=:secret";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $_SESSION['id']);
		$stm->bindParam(':secret', $_SESSION['secret']);
		$stm->execute();
		$res = $stm->fetch();

		if ($res) {
			array_walk_recursive($res, 'sanitize');
			
			foreach ($res as $k => $v) {
				if (property_exists($this, $k)) {
					$this->$k = $v;
				}
			}
			
			$this->permissions = explode('|', $this->permissions);
			$this->is_signed_in = true;
		}
	}
	
	function create () {
		$password = password_hash($this->password, PASSWORD_DEFAULT);
		$this->permissions = implode('|', $this->permissions);
		
		$sql = "insert into `users` (`username`, `password`, `level`, `permissions`) values (:username, :password, :level, :permissions)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':username', $this->username);
		$stm->bindParam(':password', $password);
		$stm->bindParam(':level', $this->level);
		$stm->bindParam(':permissions', $this->permissions);
		$stm->execute();
		
		$this->id = $this->db->lastInsertId();
	}
	
	function update () {
		$this->permissions = implode('|', $this->permissions);
		
		$update_sql = "update `users` set `username`=:username, `secret`='', `level`=:level, `permissions`=:permissions";
		if (!empty($this->password)) {
			$update_sql .= ", `password`=:password";
		}
		$sql = $update_sql." where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':username', $this->username);
		$stm->bindParam(':level', $this->level);
		$stm->bindParam(':permissions', $this->permissions);
		if (!empty($this->password)) {
			$password = password_hash($this->password, PASSWORD_DEFAULT);
			$stm->bindParam(':password', $password);
		}
		$stm->bindParam(':id', $this->id);
		$stm->execute();
	}

	function delete () {
		$sql = "update `users` set `deleted`=1 where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
	}
}

?>