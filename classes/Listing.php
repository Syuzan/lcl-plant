<?php

class Listing {		
	function __construct () {
	}

	function userLevels () {
		$user_levels = array();	
		$user_levels['Admin'] = 'Admin';
		$user_levels['User'] = 'User';

		return $user_levels;
	}
	
	function userPermissions () {
		$user_permissions = array();			
		$user_permissions['view_teams'] = 'view_teams';
		$user_permissions['create_teams'] = 'create_teams';
		$user_permissions['update_team_notes'] = 'update_team_notes';
		$user_permissions['delete_teams'] = 'delete_teams';
		$user_permissions['create_team_objects'] = 'create_team_objects';
		$user_permissions['delete_team_objects'] = 'delete_team_objects';

		return $user_permissions;		
	}
}

?>