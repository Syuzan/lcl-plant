<?php

class Vehicle {
	public $db;
	public $user;
	public $id;
	public $make;
	public $model;
	public $type;
	public $reg;

	function __construct ($db, $user) {
		$this->db = $db;
		$this->user = $user;
	}

	function populateByPost () {
		array_walk_recursive($_POST, 'desanitize');
		
		foreach ($_POST as $k => $v) {
			if (property_exists($this, $k)) {
				$this->$k = $v;
			}
		}
	}

	function populateById () {
		$sql = "select * from `vehicles` where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
		$res = $stm->fetch();

		if ($res) {
			array_walk_recursive($res, 'sanitize');
			
			foreach ($res as $k => $v) {
				$this->$k = $v;
			}
		}
	}

	function create () {
		$sql = "insert into `vehicles` (`make`, `model`, `type`, `reg`) values (:make, :model, :type, :reg)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':make', $this->make);
		$stm->bindParam(':model', $this->model);
		$stm->bindParam(':type', $this->type);
		$stm->bindParam(':reg', $this->reg);
		$stm->execute();

		$this->id = $this->db->lastInsertId();
	}

	function update () {
		$sql = "update `vehicles` set `make`=:make, `model`=:model, `type`=:type, `reg`=:reg where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':make', $this->make);
		$stm->bindParam(':model', $this->model);
		$stm->bindParam(':type', $this->type);
		$stm->bindParam(':reg', $this->reg);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
	}

	function delete () {
		$sql = "update `vehicles` set `deleted`=1 where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
	}
}

?>