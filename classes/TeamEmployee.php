<?php

class TeamEmployee {
	public $db;
	public $user;
	public $id;
	public $id_team;
	public $id_employee;

	function __construct ($db, $user) {
		$this->db = $db;
		$this->user = $user;
	}
	
	function exists () {
		$sql = "select count(*) from `team_employees` where `id_team`=:id_team and `id_employee`=:id_employee";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_team', $this->id_team);
		$stm->bindParam(':id_employee', $this->id_employee);
		$stm->execute();
		$res = $stm->fetchColumn();

		return $res;
	}

	function create () {
		$sql = "insert into `team_employees` (`id_team`, `id_employee`) values (:id_team, :id_employee)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_team', $this->id_team);
		$stm->bindParam(':id_employee', $this->id_employee);
		$stm->execute();

		$this->id = $this->db->lastInsertId();
	}

	function delete () {
		$sql = "delete from `team_employees` where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
	}
}

?>