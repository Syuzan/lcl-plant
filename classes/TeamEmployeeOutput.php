<?php

class TeamEmployeeOutput {
	public $db;
	public $user;
	public $id_team;
	public $main_sql = "select te.*, e.name, t.id_location
						from `team_employees` as te
						left join `employees` as e on (e.id = te.id_employee)
						left join `teams` as t on (t.id = te.id_team)
						where te.id_team=:id_team";
	public $rows = array();
	public $row;

	function __construct ($db, $user, $id_team, $print = false) {
		$this->db = $db;
		$this->user = $user;
		$this->id_team = $id_team;
		$this->print = $print;
	}

	function fetchRows () {
		$sql = $this->main_sql." order by e.name asc";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_team', $this->id_team);
		$stm->execute();
		$res = $stm->fetchAll();

		if ($res) {
			array_walk_recursive($res, 'sanitize');			
			$this->rows = $res;
		}
	}

	function showTags () {
		foreach ($this->rows as $this->row) {
			?>
			<?php if (!$this->print) { ?><a href="ajax.php?table=team_employees&method=delete&id=<?php echo $this->row['id']; ?>&id_location=<?php echo $this->row['id_location']; ?>&id_team=<?php echo $this->row['id_team']; ?>&id_employee=<?php echo $this->row['id_employee']; ?>" data-team-employee="<?php echo $this->row['id']; ?>" title="Delete from Team" class="btn btn-danger btn-sm delete-team-player-link"><?php } ?><i class="fa fa-user"></i> <?php echo $this->row['name']; ?><?php if (!$this->print) { ?> <i class="fa fa-close"></i></a><?php } else { ?><br><?php } ?>
			<?php
		}
	}
}

?>