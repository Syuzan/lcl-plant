<?php

class QualificationOutput {
	public $db;
	public $user;
	public $main_sql = "select * from `qualifications`;";
	public $rows;
	public $row;

	function __construct ($db, $user) {
		$this->db = $db;
		$this->user = $user;
	}

	function count_quantity($id_employee){
		$cq = $this->license_dates_id($id_employee);
		$in_90 = 0;
		$in_30 = 0;
		$drn = 0;
		foreach ($cq as $key => $value) {
			if(!empty($value["received_date"])){
				$drn = $this->days_diff($value["received_date"],$value["expiration_date"]);

				if($drn && $drn<=30 && $drn>0){
					$in_30++;
				}
				elseif ($drn && $drn<=90 && $drn>0) {
					$in_90++;
				}
			}
			else{
				$drn = 0;
				$in_90 = 0;
				$in_30 = 0;
			}
			
		}
		return array('in_30' => $in_30, 'in_90' => $in_90, 'duration' => $drn, );

	}

	function days_diff($d1,$d2){
		$datetime1 = new DateTime("$d1");
		$datetime2 = new DateTime("$d2");
		$interval = $datetime1->diff($datetime2);
		return $interval->format('%a');
	}

	function validateDate($date, $format = 'Y-m-d H:i:s')
	{
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	function select_emp_by_id($emp_id){
		$sql = "SELECT * FROM employees where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $emp_id);
		$stm->execute();
		$res = $stm->fetch();
		return $res;
	}

	function select_emp_last_date($emp_id){
		$sql = "select created_date from employees where `id`=:id order by created_date asc limit 1;";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $emp_id);
		$stm->execute();
		$res = $stm->fetch();
		return $res;
	}

	function select_qu_last_date($emp_id){
		$sql = "select received_date from employee_qualification where `id`=:id order by received_date asc limit 1;";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $emp_id);
		$stm->execute();
		$res = $stm->fetch();
		return $res;
	}

	function select_upl_last_date($emp_id){
		$sql = "select upload_date from uploads where `id_employee`=:id order by upload_date asc limit 1;";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $emp_id);
		$stm->execute();
		$res = $stm->fetch();
		return $res;
	}

	function data_select () {

		$sql = "SELECT *,g.id as emp_id, COUNT(m.id_qualification) AS count_qu
				FROM employees AS g
				LEFT JOIN employee_qualification AS m ON g.id = m.id_employee
				WHERE g.deleted = 0
				GROUP BY g.id";
		$stm = $this->db->prepare($sql);
		$stm->execute();
		$res = $stm->fetchAll();

		if ($res) {
			array_walk_recursive($res, 'sanitize');
			$this->rows = $res;
		}
	}

	function count_qualif_by_id ($id_employee) {

		$sql = "SELECT *,g.id as emp_id, COUNT(m.id_qualification) AS count_qu
				FROM employees AS g
				LEFT JOIN employee_qualification AS m ON g.id = m.id_employee
				WHERE g.id=:id_employee";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_employee', $id_employee);
		$stm->execute();
		$res = $stm->fetch();
		return $res;

	}

	function license_dates_id ($id_employee) {
		$sql = "SELECT * FROM `employee_qualification` where `id_employee`=:id_employee";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_employee', $id_employee);
		$stm->execute();
		$res = $stm->fetchAll();
		return $res;
	}

	function license_names_by_id ($id_employee) {
		$sql = "SELECT * 
				FROM `employee_qualification` as t1 
				LEFT JOIN `qualifications` as t2 
				ON t2.id = t1.id_qualification
				where `id_employee`=:id_employee";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_employee', $id_employee);
		$stm->execute();
		$res = $stm->fetchAll();
		return $res;
	}
	function get_thirty_days($id_employee){
		$sql = "SELECT em_q.*,q.name
				FROM `employee_qualification` as em_q
				LEFT JOIN `qualifications` as q 
				ON(em_q.id_qualification=q.id)
				WHERE `id_employee` = :id_employee
				AND ABS(DATEDIFF(DATE_FORMAT(em_q.received_date,'%Y-%m-%d'),em_q.expiration_date)) <= 30
				ORDER BY em_q.expiration_date ASC";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_employee', $id_employee);
		$stm->execute();
		$res = $stm->fetchAll();
		return $res;
	}
	function get_ninety_days($id_employee){
		$sql = "SELECT em_q.*,q.name
				FROM employee_qualification as em_q
				LEFT JOIN qualifications as q ON(q.id=em_q.id_qualification)
				WHERE `id_employee` = :id_employee
				AND (ABS(DATEDIFF(DATE_FORMAT(em_q.received_date,'%Y-%m-%d'),em_q.expiration_date)) <= 90 
				AND ABS(DATEDIFF(DATE_FORMAT(em_q.received_date,'%Y-%m-%d'),em_q.expiration_date)) >30)
				ORDER BY em_q.expiration_date ASC";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_employee', $id_employee);
		$stm->execute();
		$res = $stm->fetchAll();
		return $res;
	}
	function get_expired_licenses($id_employee){
		$sql = "SELECT em_q.*, q.name
				FROM employee_qualification AS em_q
				LEFT JOIN qualifications AS q ON (em_q.id_qualification = q.id)
				WHERE id_employee = :id_employee
				AND DATEDIFF(em_q.expiration_date,DATE_FORMAT(em_q.received_date,'%Y-%m-%d')) <= 0
				ORDER BY em_q.expiration_date ASC";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_employee', $id_employee);
		$stm->execute();
		$res = $stm->fetchAll();
		return $res;
	}
	function fetchRows () {
		
		$sql = $this->main_sql;
		$stm = $this->db->prepare($sql);
		$stm->execute();
		$res = $stm->fetchAll();

		if ($res) {
			array_walk_recursive($res, 'sanitize');
			$this->rows = $res;
		}
	}

	function fetchRowsCount () {

		$sql = "SELECT COUNT(DISTINCT`id_employee`), FROM `employee_qualification` WHERE 1";
		$stm = $this->db->prepare($sql);
		$stm->execute();
		$res = $stm->fetchAll();

		if ($res) {
			array_walk_recursive($res, 'sanitize');
			$this->rows = $res;
		}
	}

	function fetchRow ($id) {
		$sql = $this->main_sql." and `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $id);
		$stm->execute();
		$res = $stm->fetch();

		array_walk_recursive($res, 'sanitize');		
		$this->row = $res;
	}

	function showTable () {
		?>
		<div class="table-responsive">
			<table id="table-qualifications" class="table table-hover">
				<thead>
					<tr>
						<th>Name</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php $this->showRows(); ?>
				</tbody>
			</table>
		</div>
		<?php
	}

	function showRows () {
		if (empty($this->rows)) {
			?>
			<tr id="row-empty">
				<td colspan="100">No qualifications to display.</td>
			</tr>
			<?php
		}
		else {
			foreach ($this->rows as $this->row) {
				$this->showRow();
			}
		}
	}

	function showRow () {
		?>
		<tr id="row-<?php echo $this->row['id']; ?>">
			<td><?php echo $this->row['name']; ?></td>
			<td class="text-right nowrap">
				<a href="ajax.php?table=employees&method=modal&action=update&id=<?php echo $this->row['id']; ?>" title="Update" class="btn btn-primary modal-link"><i class="fa fa-pencil"></i></a>
				<a href="ajax.php?table=employees&method=delete&id=<?php echo $this->row['id']; ?>" data-confirm="Are you sure you want to delete <?php echo $this->row['name']; ?>?" title="Delete" class="btn btn-danger delete-link"><i class="fa fa-trash"></i></a>
			</td>
		</tr>
		<?php
	}
	
	function showTags () {
		foreach ($this->rows as $this->row) {
			?>
			<a href="#" class="btn btn-primary btn-sm" data-employee="<?php echo $this->row['id']; ?>"><i class="fa fa-user"></i> <?php echo $this->row['name']; ?></a>
			<?php
		}
	}
	
	function modalCreate ($emp_id) {
		?>

		<?php
			$l = new Qualifications($this->db, $this->user);
			$qlfs = $l->select();
			$upl = new Uploads($this->db, $this->user);
			$upls = $upl->select_by_id($emp_id);
			$up_count = $upl->uploads_count($emp_id);
			$emp_data = self::count_quantity($emp_id);
			$usr_data = self::select_emp_by_id($emp_id);
			$count_total = self::count_qualif_by_id($emp_id);
			$thirty = $this->get_thirty_days($emp_id);
			$ninety = $this->get_ninety_days($emp_id);
			$expired_licenses = $this->get_expired_licenses($emp_id);
		?>
		<div class="modal" tabindex="-1" id = "employee_modal" data-emp-id="<?php echo $emp_id;?>" role="dialog" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">
							<?= (isset($usr_data)) ? $usr_data['name'] : "Qualifications"; ?>
						</h4>
					</div>
					
					<div class="modal-body">
						<table id="mytable2">
							<tr>
								<th>Total</th>
								<th class = "thirty" data-toggle="modal" data-target="#show_30">30 Days</th>
								<th class =  "ninety">90 Days</th>
								<th>Uploads</th>
							</tr>
							<tr>
								<td><?php echo $count_total['count_qu']; ?></td>
								<td><?php echo $emp_data['in_30']; ?>
									<div class = "show_thirty">
										<table>
											<tr>
												<th>Name</th>
												<th>Expiration Date</th>
											</tr>
											<?php
												foreach ($thirty as $key1 => $value1) {
													echo "<tr><td>".$value1['name']."</td><td>".$value1['expiration_date']."</td></tr>";
												}
											?>
										</table>
									</div>
								</td>
								<td><?php echo $emp_data['in_90']; ?>
									<div class = "show_ninety">
										<table>
											<tr>
												<th>Name</th>
												<th>Expiration Date</th>
											</tr>
											<?php
												foreach ($ninety as $key2 => $value2) {
													echo "<tr><td>".$value2['name']."</td><td>".$value2['expiration_date']."</td></tr>";
												}
											?>
										</table>
									</div>
								</td>
								<td><?php echo $up_count; ?></td>
							</tr>
						</table>	

						<div class = "show_date">
							<p> Select Date</p>
							<p class = "error_msg"></p>
							<input type = "hidden" name = "cheched_license" class = "cheched_license">
							<p><input type="text" class="popup_btn popup_date datepicker" name="date" placeholder="Add Training Modules"></p>
							<p><button type="button" class="popup_btn save_license">Save</button></p>
						</div>
						<h4> Licenses</h4>
							
						<?php
							if(null !== (self::license_dates_id($emp_id))){
								$emp_quals = self::license_dates_id($emp_id);
							}

							foreach ($emp_quals as $key => $value) {
								$emp_quals_ids[] = $value['id_qualification'];
							}
							$str_table = "";
							$columns = 4;
							$a = sizeof($qlfs);
							$rows = ceil($a / $columns);

							$str_table .= '<div class="border_div"><table id="mytable3">';

							for ($row = 0; $row < $rows; $row++) {
							    $str_table .= '<tr>';

							    for ($i=0; $i<$a; $i++) {
							        if ($i % $rows == $row) {
							        	$chk = '';
							        	$disable = "";
							        	$delete = '';
							        	if(isset($emp_quals_ids) ){
								        	foreach ($emp_quals_ids as $key => $value) {
								        		if($value==$qlfs[$i]["id"]){
								        			$chk = "checked";
								        			$disable = "disabled";
								        		}
								        	}
								        }
							            $str_table .= '<td><input class = "licenses" data-empl = "'.$emp_id.'" data-confirm="Are you sure you want to delete?" type="checkbox" '.$chk.' name="checkbox" value="'.$qlfs[$i]["id"].'"><div class="popup_td_div3"> '.$qlfs[$i]["name"].'</div></td>';
							        }
							    }

							    $str_table .= '</tr>';
							}
							$str_table .= '</table></div>';
							echo $str_table;
						?>
						<script type="text/javascript">
							$('.datepicker').datepicker({
								autoclose: true,
								orientation: 'bottom right'		
							})
						</script>

							<div class="popup_div show_images">Uploads</div>
							<div class="popup_div activate_email">Email</div>
							<br>
							<form method="post" action="#" id="sendEmail_form" class="form-horizontal modal-form" enctype="multipart/form-data">
							<p class = "error_msg"></p>
							<?php
								$start = 1;
								$end = sizeof($upls);
								$str_table = "";

								$str_table .= '<div class="border_div"><div class = "row" id="mytable4"><div class = "col-md-12">';
								foreach (range($start, $end) as $i)
								{
							    	if(isset($upls[$i-1])){
									    $str_table .= '<div class = "col-md-3 current_item"><input type="checkbox" name="selected_img[]" class="choose_img" value="'.$upls[$i-1]["directory"].$upls[$i-1]["file_name"].'"><div data-file-id="'.$upls[$i-1]["id"].'" class="popup_td_div4">'.$upls[$i-1]["name"].'</div> <a href="'.$upls[$i-1]["directory"].$upls[$i-1]["file_name"].'" target="_blank"><span class="span_btn span_show">&#128269</span></a> <span class="span_btn span_close" data-confirm="Are you sure you want to delete?">&#10006</span></div>';
								    }
								    else{
								    	$str_table = '<div class="border_div"><div class = "row" id="mytable4"><div class = "col-md-12">No files to show.';
								    }
								}

								$str_table .= '</div></div>';
								$str_table .= '<div class = "email_div"><input type = "email" name = "email_to" placeholder = "Enter email here" class = "email_to">';
								$str_table .= '<button type="button" class="send_email popup_btn">Send</button></div>';
								$str_table .= '</div>';
								echo $str_table;

							?>
								
							</form>
						<form method="post" action="#" id="modal_form_id" class="form-horizontal modal-form" enctype="multipart/form-data">
							<input type="text" class="empl_id" hidden="true" name="emp_id" value="<?php echo $emp_id ;?>">
							<div class = "upload_content">
								<input type="file" id="file" name="file[]" multiple="multiple" />
								<br>

								<i class="fa fa-upload" aria-hidden="true"></i>
								<input id="popup_browse" type="button" class="popup_browse_btn popup_btn" value="Browse">
								<label id="browse_label"> 0 files selected</label>
							</div>
							<div class="popup_div_btn">
								<button type="button" class="popup_save_btn popup_btn save_and_next">Save & Next</button>
								<button type="button" class="popup_save_btn popup_btn save_qualification">Save</button>
								<div class="clearfix"></div>
							</div>
						</form>
							<div class = "history">
								<h4>Expired licenses</h4>
								<?php
									if(count($expired_licenses) > 0){
									?>
										<table id = "expired_license">
											<tr>
												<th>License name</th>
												<th>Expired date</th>
											</tr>
									<?php
										foreach ($expired_licenses as $license) {
											echo "<tr><td>".$license['name']."</td><td>".$license['expiration_date']."</td></tr>";
										}
									?>
										</table>
									<?php
									}else{
										echo "<p class='no_expired'>No expired licenses</p>";
									}
								?>
							</div>
							<span class="popup_span">
								Last modified:
								<?php
									if(null !== (self::select_upl_last_date($emp_id))){
										$modified1 = substr(self::select_upl_last_date($emp_id)['upload_date'],0,10);
									}
									if(null !== (self::select_qu_last_date($emp_id))){
										$modified2 = substr(self::select_qu_last_date($emp_id)['received_date'],0,10);
									}
									if($modified1 && $modified2){
										if(strtotime($modified1) > strtotime($modified2)){
											echo "<span class = 'modified'>$modified1</span>";
										}
										else{
											echo "<span class = 'modified'>$modified2</span>";
										}
									}
									else{
										if($modified1){
											echo "<span class = 'modified'>$modified1</span>";
										}
										else{
											echo "<span class = 'modified'>$modified2</span>";
										}
									}
								?>

								| Created 
								 
								<?php 
									if(null!== (self::select_emp_last_date($emp_id))){
										$created = substr(self::select_emp_last_date($emp_id)['created_date'],0,10);
									}
									echo "<span class = 'create'>$created</span>";
								?>
							</span>
						<script type="text/javascript">

						    $(document).ready(function() {
						    	$(".span_close").unbind('click');
						    	$('#employee_modal').on('hidden.bs.modal', function () {
						    		$(".span_close").unbind('click');
						    	})
						    	var usr = "<?= (isset($usr_data)) ? $usr_data['name'] : ''; ?>";
						    	var v_tr = $('#myTable tbody tr');
						    	var v_a = '';
						    	var v_current = '';
						    	var employee_by_name = '';
						    	var next_employee_id = '';
						    	v_tr.each(function(){

						    		v_a = $(this).children('td:nth-child(1)').find('a').html();
						    		if(v_a==usr){
						    			v_current = $(this).children('td:nth-child(5)');
						    			v_current.html(us_count);
						    		}
						    	});
						    	
								$('#popup_browse').on("click", function(){
									$( "#file" ).trigger( "click" );
								});
								$('#file').change("click", function(){
									var numFiles = $("input:file")[0].files.length;
									numFiles += ' files selected';
									$('#browse_label').text(numFiles);
								});
								var  asd = '<?php echo $emp_id ; ?>';
						        
						        var us_count = $("#mytable2").find("tbody tr:nth-child(2) td:nth-child(4)").html();
								$( ".span_close" ).bind( "click", function( e ) {
								    e.preventDefault();
									if (confirm($(this).data('confirm')) == true) {
							        	us_count--;
							        	$("#mytable2").find("tbody tr:nth-child(2) td:nth-child(4)").html(us_count);
							        	v_current.html(us_count);

								        var c =  $('#mytable4 .current_item').length;
								        var b = Math.ceil(b = c/4);
										$(this).parent('.current_item').remove();
										var up_id = $(this).closest('.current_item').find('div').data('file-id');
						        		$.ajax({
											method:"POST",
											url: 'ajax.php?table=qualification&method=modal&action=delete',
											data: { up_id : up_id},
											success: function(data){

											}
										});
									} 
								});
						        
								var a = [];
						        $("input[name=check_box]:checked").each(function(){
								    a.push($(this).val());
								});

								var calendar = '';
						    });
							$(document).one("click",'.popup_btn', function(){
								if($(this).hasClass("datepicker")){
									var checked = false;
									calendar = $('.datepicker-dropdown');
									if(!$('.never_expire').length){
										calendar.append('<input class="never_expire" type="checkbox" name="checkbox" value="" > Never expire');
									}

									$('.never_expire').on('click',function(e){
										e.stopPropagation();
										$('.datepicker-dropdown').hide();
										if(!checked){
											$("input.datepicker:text").val('Never Expires');
											checked = true;
										}
										else if(checked && $("input.datepicker:text").val()=='Never Expires'){
											$("input.datepicker:text").val('Add Training Modules');
											checked = false;
										}
									});
									$(this).change(function(){
										if($(this).val()!='Never Expires' || jQuery.type( $(this).val() ) === "date"){
											if(checked){
												$('.never_expire').click();
											}
											checked = false;
										}
									});
								}
							});
						</script>

							<?php
							$fo = new FormOutput($this->db, $this->user);
							
							?>
					</div>
				</div>
			</div>
		</div>
		<?php
	}

	function modalUpdate () {
		?>
		<div class="modal" tabindex="-1" role="dialog" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Update Employee</h4>
					</div>
					<div class="modal-body">
						<form method="post" action="ajax.php?table=employees&method=update&id=<?php echo $this->row['id']; ?>" class="form-horizontal modal-form">
							<?php
							$fo = new FormOutput($this->db, $this->user);
							$fo->showInput('text', 'Name', 'name', $this->row['name'], true);
							$fo->showSubmit('Update');
							?>
						</form>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
}

?>