<?php

class TeamVehicleOutput {
	public $db;
	public $user;
	public $id_team;
	public $main_sql = "select tv.*, v.make, v.model, v.type, v.reg, t.id_location
						from `team_vehicles` as tv
						left join `vehicles` as v on (v.id = tv.id_vehicle)
						left join `teams` as t on (t.id = tv.id_team)
						where tv.id_team=:id_team";
	public $rows = array();
	public $row;

	function __construct ($db, $user, $id_team, $print = false) {
		$this->db = $db;
		$this->user = $user;
		$this->id_team = $id_team;
		$this->print = $print;
	}

	function fetchRows () {
		$sql = $this->main_sql." order by v.model, v.type, v.reg asc";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_team', $this->id_team);
		$stm->execute();
		$res = $stm->fetchAll();

		if ($res) {
			array_walk_recursive($res, 'sanitize');			
			$this->rows = $res;
		}
	}

	function showTags () {
		foreach ($this->rows as $this->row) {
			?>
			<?php if (!$this->print) { ?><a href="ajax.php?table=team_vehicles&method=delete&id=<?php echo $this->row['id']; ?>&id_location=<?php echo $this->row['id_location']; ?>&id_team=<?php echo $this->row['id_team']; ?>&id_vehicle=<?php echo $this->row['id_vehicle']; ?>" data-team-vehicle="<?php echo $this->row['id']; ?>" title="Delete from Team" class="btn btn-danger btn-sm delete-team-player-link"><?php } ?><i class="fa fa-car"></i> <?php echo $this->row['make'].' '.$this->row['model'].' '.$this->row['reg']; ?><?php if (!$this->print) { ?> <i class="fa fa-close"></i></a><?php } else { ?><br><?php } ?>
			<?php
		}
	}
}

?>