<?php

class TeamVehicle {
	public $db;
	public $user;
	public $id;
	public $id_team;
	public $id_vehicle;

	function __construct ($db, $user) {
		$this->db = $db;
		$this->user = $user;
	}
	
	function exists () {
		$sql = "select count(*) from `team_vehicles` where `id_team`=:id_team and `id_vehicle`=:id_vehicle";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_team', $this->id_team);
		$stm->bindParam(':id_vehicle', $this->id_vehicle);
		$stm->execute();
		$res = $stm->fetchColumn();

		return $res;
	}

	function create () {
		$sql = "insert into `team_vehicles` (`id_team`, `id_vehicle`) values (:id_team, :id_vehicle)";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id_team', $this->id_team);
		$stm->bindParam(':id_vehicle', $this->id_vehicle);
		$stm->execute();

		$this->id = $this->db->lastInsertId();
	}

	function delete () {
		$sql = "delete from `team_vehicles` where `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $this->id);
		$stm->execute();
	}
}

?>