<?php

class VehicleOutput {
	public $db;
	public $user;
	public $main_sql = "select * from `vehicles` where `deleted`=0";
	public $rows;
	public $row;

	function __construct ($db, $user) {
		$this->db = $db;
		$this->user = $user;
	}

	function fetchRows ($exclude_teams = null) {
		$where_sql = "";
		if ($exclude_teams != null) {
			$where_sql = " and `id` not in (select `id_vehicle` from `team_vehicles` where `id_team` in ($exclude_teams)) ";
		}
		
		$sql = $this->main_sql." 
				".$where_sql."
				order by `make`, `model`, `type`, `reg` asc";
		$stm = $this->db->prepare($sql);
		$stm->execute();
		$res = $stm->fetchAll();

		if ($res) {
			array_walk_recursive($res, 'sanitize');			
			$this->rows = $res;
		}
	}

	function fetchRow ($id) {
		$sql = $this->main_sql." and `id`=:id";
		$stm = $this->db->prepare($sql);
		$stm->bindParam(':id', $id);
		$stm->execute();
		$res = $stm->fetch();

		array_walk_recursive($res, 'sanitize');		
		$this->row = $res;
	}

	function showTable () {
		?>
		<div class="table-responsive">
			<table id="table-vehicles" class="table table-hover">
				<thead>
					<tr>
						<th>Make</th>
						<th>Model</th>
						<th>Type</th>
						<th>Reg</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php $this->showRows(); ?>
				</tbody>
			</table>
		</div>
		<?php
	}

	function showRows () {
		if (empty($this->rows)) {
			?>
			<tr id="row-empty">
				<td colspan="100">No vehicles to display.</td>
			</tr>
			<?php
		}
		else {
			foreach ($this->rows as $this->row) {
				$this->showRow();
			}
		}
	}

	function showRow () {
		?>
		<tr id="row-<?php echo $this->row['id']; ?>">
			<td><?php echo $this->row['make']; ?></td>
			<td><?php echo $this->row['model']; ?></td>
			<td><?php echo $this->row['type']; ?></td>
			<td><?php echo $this->row['reg']; ?></td>
			<td class="text-right nowrap">
				<a href="ajax.php?table=vehicles&method=modal&action=update&id=<?php echo $this->row['id']; ?>" title="Update" class="btn btn-primary modal-link"><i class="fa fa-pencil"></i></a>
				<a href="ajax.php?table=vehicles&method=delete&id=<?php echo $this->row['id']; ?>" data-confirm="Are you sure you want to delete <?php echo $this->row['make'].' '.$this->row['model'].' '.$this->row['reg']; ?>?" title="Delete" class="btn btn-danger delete-link"><i class="fa fa-trash"></i></a>
			</td>
		</tr>
		<?php
	}
	
	function showTags () {
		foreach ($this->rows as $this->row) {
			?>
			<a href="#" class="btn btn-primary btn-sm" data-vehicle="<?php echo $this->row['id']; ?>"><i class="fa fa-car"></i> <?php echo $this->row['make'].' '.$this->row['model'].' '.$this->row['reg']; ?></a>
			<?php
		}
	}

	function modalCreate () {
		?>
		<div class="modal" tabindex="-1" role="dialog" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Create Vehicle</h4>
					</div>
					<div class="modal-body">
						<form method="post" action="ajax.php?table=vehicles&method=create" class="form-horizontal modal-form">
							<?php
							$fo = new FormOutput($this->db, $this->user);
							$fo->showInput('text', 'Make', 'make', '', true);
							$fo->showInput('text', 'Model', 'model', '', true);
							$fo->showInput('text', 'Type', 'type', '', true);
							$fo->showInput('text', 'Reg', 'reg', '', true);
							$fo->showSubmit('Create');
							?>
						</form>
					</div>
				</div>
			</div>
		</div>
		<?php
	}

	function modalUpdate () {
		?>
		<div class="modal" tabindex="-1" role="dialog" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Update Vehicle</h4>
					</div>
					<div class="modal-body">
						<form method="post" action="ajax.php?table=vehicles&method=update&id=<?php echo $this->row['id']; ?>" class="form-horizontal modal-form">
							<?php
							$fo = new FormOutput($this->db, $this->user);
							$fo->showInput('text', 'Make', 'make', $this->row['make'], true);
							$fo->showInput('text', 'Model', 'model', $this->row['model'], true);
							$fo->showInput('text', 'Type', 'type', $this->row['type'], true);
							$fo->showInput('text', 'Reg', 'reg', $this->row['reg'], true);
							$fo->showSubmit('Update');
							?>
						</form>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
}

?>