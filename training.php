<?php

include 'includes/init.php';

if (!$user->is_signed_in || $user->level != 'Admin') {
	redirect('index.php');
}

$page['title'] = 'Training';

include 'header.php';

?>
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">




<table id="myTable">
	<thead><tr><th>Name</th><th>Qualify</th><th>30 Days</th><th>90 Days</th><th>Uploads</th></tr></thead>
	<tfoot><tr><th>Name</th><th>Qualify</th><th>30 Days</th><th>90 Days</th><th>Uploads</th></tr></tfoot>
</table>



<?php

$qo = new QualificationOutput($db, $user);
$qo->fetchRows();

	$in_30 = 0;
	$in_90 = 0;
	$duration=0;


$count = new QualificationOutput($db, $user);
$count->data_select();

$upl = new Uploads($db, $user);



include 'footer.php';

?>

<script type="text/javascript">
	
	$(document).ready(function(){
	    $('#myTable').DataTable();
	});



	$('#myTable').dataTable( {
		aLengthMenu: [
	        [60, 120, 240, -1],
	        [60, 120, 240, "All"]
	    ],
	    "data": [
	    <?php foreach ($count->rows as $key => $value): ?>
	    	<?php
	    	if($value["id_employee"]=="")
	    		$value["id_employee"]=0;
			$in_90 = 0;
			$in_30 = 0;
			$duration = 0;
	    	$arr = $qo->count_quantity($value["emp_id"]);
	    	$up_count = $upl->uploads_count($value["emp_id"]);
	    	$in_30 = $arr['in_30'];
	    	$in_90 = $arr['in_90'];
	    	$duration = $arr['duration'];
	    	?>
	        {
	            "name":       "<input type='button' class='tb_btn' value='+'> <a data-emp-id='<?php echo $value['emp_id']; ?>' href='ajax.php?table=qualification&method=modal&action=create&emp_id=<?php echo $value["emp_id"]; ?>' title='Qualification' class='tb_link modal-link'><?php echo $value["name"]; ?></a>",
	            "position":   "<?php echo $value["count_qu"]; ?>",
	            "in_30":      "<?php echo $in_30; ?>",
	            "in_90":      "<?php echo $in_90; ?>",
	            "start_date": "<?php echo $up_count; ?>",
	        },
	    <?php endforeach; ?>    
	    ],
	    "columns": [
	        { "data": "name" },
	        { "data": "position" },
	        { "data": "in_30" },
	        { "data": "in_90" },
	        { "data": "start_date" }
	    ]
	} );

	$("#more_info").on("click", function(){

	});

</script>
<style type="text/css">
	#myTable{
		width: 1305px !important;
		color: #000;
		text-transform: uppercase;
	}
	#myTable_wrapper{
		margin: 2px;
	    background: #4e5d6c;
    	border: 3px solid #37495a;
	}
	#myTable tr:nth-child(odd),table.dataTable tbody tr {
		background-color: #2b3e50;
	}
	table.dataTable tbody th, table.dataTable tbody td{
		color: #fff;
    	border-top: 1px solid #4e5d6c;
	}
	#myTable th {
	    color: #fff;
		background-color: #df691a;
	}
	.dataTables_wrapper .dataTables_filter input{
	    background: #e4e1e1;
	}
	.tb_btn{
		border: none;
		border-radius: 10px;
		background: #df691a;
	    color: #fff;
		font-weight: bold;
	}
	.dataTables_info{
		color: #fff !important;
	}
	#myTable_previous,#myTable_next {
		color: #fff !important;
	}
	.tb_link{
		text-decoration: none !important;
		color: #fff !important;
	}
</style>

