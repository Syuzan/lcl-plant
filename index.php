<?php

include 'includes/init.php';

if ($user->is_signed_in) {
	redirect('dashboard.php');
}

if (!empty($_POST)) {
	$user->populateByPost();
	
	if ($user->credentialsValid()) {
		$user->signIn();
		redirect('dashboard.php');
	}
}

$page['title'] = 'Sign In';

include 'header.php';

?>

<div class="page-header">
	<h1><?php echo $page['title']; ?></h1>
</div>

<form method="post" class="form-horizontal">
	<?php
	$fo = new FormOutput();
	$fo->showInput('text', 'Username', 'username', '', true);
	$fo->showInput('password', 'Password', 'password', '', true);
	$fo->showSubmit('Sign In');
	?>
</form>

<?php

include 'footer.php';

?>