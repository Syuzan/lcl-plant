<?php

include 'includes/init.php';

if ($user->is_signed_in) {
	$_SESSION['id'] = '';
	$_SESSION['secret'] = '';
	
	session_destroy();
}

redirect('index.php');

?>