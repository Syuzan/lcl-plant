<?php

include 'includes/init.php';

if (!$user->is_signed_in || $user->level != 'Admin') {
	redirect('index.php');
}

$page['title'] = 'Vehicles';

include 'header.php';

?>

<div class="page-header">
	<a href="ajax.php?table=vehicles&method=modal&action=create" title="Create Vehicle" class="btn btn-primary pull-right modal-link"><i class="fa fa-plus"></i></a>
	<h1><?php echo $page['title']; ?></h1>
</div>

<?php

$lo = new VehicleOutput($db, $user);
$lo->fetchRows();
$lo->showTable();

include 'footer.php';

?>