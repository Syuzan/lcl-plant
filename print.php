<?php

include 'includes/init.php';

if (!$user->is_signed_in) {
	redirect('index.php');
}

if (isset($_GET['date'])) {
	$date = $_GET['date'];
	$timestamp = strtotime($_GET['date']);
}
else {
	redirect('print.php?date='.date('m/d/Y', time()));
}

$page['title'] = date('F j, Y', $timestamp).' Teams';

?>

<!DOCTYPE html>
<html lang="en">
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title><?php echo $page['title'].' | '.APP_TITLE; ?></title>

		<link href="css/bootstrap-print.min.css" rel="stylesheet" type="text/css">
		<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">

	</head>
	<body onload="window.print();">
	
		<div class="container">

			<div class="page-header">
				<h1><?php echo $page['title']; ?></h1>
			</div>

			<?php

			$to = new TeamOutput($db, $user, true);
			$to->setDateFields($date, $timestamp);
			$to->fetchLocations();
			$to->showLocations();

			?>

		</div>

	</body>
</html>