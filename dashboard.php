<?php

include 'includes/init.php';

if (!$user->is_signed_in) {
	redirect('index.php');
}

$page['title'] = 'Dashboard';

include 'header.php';

?>

<div class="page-header">
	<h1><?php echo $page['title']; ?></h1>
</div>

<p>Welcome to the dashboard.</p>

<?php

include 'footer.php';

?>