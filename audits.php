<?php

include 'includes/init.php';

if (!$user->is_signed_in || $user->level != 'Admin') {
	redirect('index.php');
}

$page['title'] = 'Audits';

include 'header.php';

?>

<div class="page-header">
	<h1><?php echo $page['title']; ?></h1>
</div>

<?php

$ao = new AuditOutput($db, $user);
$ao->fetchRows();
$ao->showTable();

include 'footer.php';

?>