<?php

include 'includes/init.php';

$a = new Audit($db, $user);

// Qualification
if ($_GET['table'] == 'qualification') {
	$l = new Qualifications($db, $user);
	$lo = new QualificationOutput($db, $user);
	$lic_out = new LicensesOutput($db,$user);
	if ($user->level == 'Admin') {
		if ($_GET['method'] == 'modal') {
			if ($_GET['action'] == 'create') {
				$emp_id = $_GET['emp_id'];
				$lo->modalCreate($emp_id);
			}
			else if ($_GET['action'] == 'create_license') {
				$lic_out->createLicense();
			}
			else if ($_GET['action'] == 'update_lic') {
				$lic_out->fetchRow($_GET['id']);
				$lic_out->modalUpdate();

			}
			else if ($_GET['action'] == 'update') {
				
				$exp_date = $_POST["date"];
				if($exp_date == 'Never Expires'){
					$exp_date = '01/01/2030';
				}
				$current_date = date('m/d/Y', time());
				$boool = $lo->validateDate($exp_date, 'm/d/Y');
				$qu_count = 0;
				$qu_count_updates = 0;
				$days_count = 0;
				if($boool && isset($_POST["emp_id"])){

					$days_count = $lo->days_diff($exp_date, $current_date);
					$exp_date = date('Y-m-d', strtotime(str_replace('-', '/', $exp_date)));
					$emp_id = $_POST["emp_id"];

					if(isset($_POST['checkbox'])){
						$checkbox = $_POST['checkbox'];
						$eq = new EmployeeQualification($db, $user, $emp_id, $checkbox, $exp_date);
						$res = $eq->check_qualif_exists();
						if(!$res){
							$eq->create();
							$qu_count++;
						}
						else{
							$eq->update();
							$qu_count_updates++;
						}
							
					}
					
				};

				$data = array('success' => 'Form was submitted', 'formData' => $_POST);
				$data['qu_count'] = $qu_count;
				$data['qu_count_updates'] = $qu_count_updates;
				$data['days_count'] = $days_count;
				echo json_encode($data);
			}
			elseif ($_GET['action'] == 'upload') {
				$ul = new Uploads($db, $user);
				$data = array();
				if(isset($_FILES['file']) && !empty($_FILES['file']))
				{  
				    $error = false;
				    $files = array();
				    $uploaddir = 'uploads/';
				    foreach($_FILES['file']['tmp_name'] as $key => $file)
				    {
				    	$count_uploaded_files = 0;
				    	$base_nm = $_FILES['file']['name'][$key];
						$ext = pathinfo($base_nm, PATHINFO_EXTENSION);
						$file_name = md5(basename($base_nm, '.'.$ext).rand());
						$file_name .= '.'.$ext;

				        if(move_uploaded_file($file, $uploaddir .basename($file_name)))
				        {
				            $files[] = $uploaddir .$file_name;
				        	$emp_id = $_POST["emp_id"];
				            $ul->create($base_nm, $uploaddir, $emp_id, $file_name);
				        }
				        else
				        {
				            $error = true;
				        }
				    }
				    $data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $files);
					echo json_encode($data);
				}else{
					$error = true;
					$data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $files);
					echo json_encode($data);
				}
			}elseif($_GET['action'] == 'send_email'){
				$email_to = isset($_POST['email_to']) ? $_POST['email_to'] : "";
				$images = isset($_POST['selected_img']) ? $_POST['selected_img'] : "";
				$str = "";
				if(!empty($email_to) && !empty($images)){
					$str .= '<html><body>';
					$str .=	'<head><link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"></head>';
					$str .=	'<div class = "container">';
					$str .=	'<div class = "row">';
					$str .=	'<div class = "col-md-12">';
					if(count($images) > 1){
						foreach ($images as $index => $img) {
							$str .= '<div class = "col-md-3"><img src = "http://www.lclplant.com/backend/'.$img.'"></div>';
						}
					}else{
						$str .= '<div class = "col-md-3"><img src = "http://www.lclplant.com/backend/'.$images[0].'"></div>';
					}
						
					$str .= '</div>';
					$str .= '</div>';
					$str .= '</body></html>';
					$subject = 'Employee image';
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'To: <'.$email_to.'>' . "\r\n";
					$headers .= 'From: Admin' . "\r\n";
					$mail = mail($email_to, $subject, $str, $headers);
					if($mail){
						echo json_encode(['success'=>'send']);die;
					}else{
						echo json_encode(['success'=>'error']);die;
					}
				}else{
					echo json_encode(['success'=>'error']);die;
				}
			}
			elseif ($_GET['action'] == 'delete') {

				$qlf = isset($_POST['qlf_id']) ? $_POST['qlf_id'] : "";
				$emp_id = isset($_POST['empl_id']) ? $_POST['empl_id'] : "";
				$up_id = isset($_POST['up_id']) ? $_POST['up_id'] : "";
				$exp_date = "";
				if(!empty($qlf) && !empty($emp_id)){
					$eq = new EmployeeQualification($db, $user, $emp_id, $qlf,$exp_date);
					$eq->delete_qlf();
					echo json_encode(['success'=>'true']);die;
				}
				else if (!empty($up_id)) {
					$ul = new Uploads($db, $user);
					$fl = $ul->select_by_file_id($up_id);
					$file_to_delete = $fl['directory'].$fl['file_name'];
					$file_to_d = $fl['name'];
					unlink($file_to_delete);
					// var_dump($ul->select_by_file_id($up_id));
					$ul->delete($up_id);
				}
				else{
					echo "Nothing to delete";
					die();
				}


				
			}
		}
		else if($_GET['method'] == 'delete_lic'){
			$l->id = $_GET['id'];
			$l->populateById();
			$l->delete();
			$a->message = 'Deleted license '.$l->name;
			$a->create();
		}
		else if($_GET['method'] == 'update_lic'){
			$name = $_POST['name'];
			$l->id = $_GET['id'];
			$l->name = $name;
			$a->before = array('name' => $l->name);
			$a->after = array('name' => $l->name);
			$l->update();
			$a->message = 'Updated license '.$l->name;
			$a->create();
			$lic_out->fetchRow($l->id);
			$lic_out->showRow();
		}
		else if($_GET['method'] == 'create_lic'){
			$name = $_POST['name'];
			$l->create_license($name);
			$lic_out->fetchRow($l->id);
			$lic_out->showRow();
		}

	}
	else if ($_GET['method'] == 'create') {
		$l->select();
	}
	else if ($_GET['method'] == 'update') {
		echo "asdasd";
		$check_box = $_GET['check_box'];
		var_dump($_GET); 
		// $l->select();
		die('asd');
	}
	else {
		echo "ddddddddd";
		die('no permission');
	}
}

// locations
if ($_GET['table'] == 'locations') {
	$l = new Location($db, $user);
	$lo = new LocationOutput($db, $user);
	
	if ($user->level == 'Admin') {
		if ($_GET['method'] == 'modal') {
			if ($_GET['action'] == 'create') {
				$lo->modalCreate();
			}
			else if ($_GET['action'] == 'update') {
				$lo->fetchRow($_GET['id']);
				$lo->modalUpdate();
			}
		}
		else if ($_GET['method'] == 'create') {
			$l->populateByPost();
			$l->create();
			
			$a->message = 'Created location '.$l->name;
			$a->create();
			
			$lo->fetchRow($l->id);
			$lo->showRow();
		}
		else if ($_GET['method'] == 'update') {
			$l->id = $_GET['id'];
			$l->populateById();
			$a->before = array('name' => $l->name);
			$l->populateByPost();
			$a->after = array('name' => $l->name);
			$l->update();
			
			$a->message = 'Updated location '.$l->name;
			$a->create();
			
			$lo->fetchRow($l->id);
			$lo->showRow();
		}
		else if ($_GET['method'] == 'delete') {
			$l->id = $_GET['id'];
			$l->populateById();
			$l->delete();
			
			$a->message = 'Deleted location '.$l->name;
			$a->create();			
		}
	}
	else {
		die('no permission');
	}
}

// employees
if ($_GET['table'] == 'employees') {
	$e = new Employee($db, $user);
	$eo = new EmployeeOutput($db, $user);
	
	if ($user->level == 'Admin') {
		if ($_GET['method'] == 'modal') {
			if ($_GET['action'] == 'create') {
				$eo->modalCreate();
			}
			else if ($_GET['action'] == 'update') {
				$eo->fetchRow($_GET['id']);
				$eo->modalUpdate();
			}
		}
		else if ($_GET['method'] == 'create') {
			$e->populateByPost();
			$e->create();
			
			$a->message = 'Created employee '.$e->name;
			$a->create();
			
			$eo->fetchRow($e->id);
			$eo->showRow();
		}
		else if ($_GET['method'] == 'update') {
			$e->id = $_GET['id'];
			$e->populateById();
			$a->before = array('name' => $e->name);
			$e->populateByPost();
			$a->after = array('name' => $e->name);
			$e->update();
			
			$a->message = 'Updated employee '.$e->name;
			$a->create();
			
			$eo->fetchRow($e->id);
			$eo->showRow();
		}
		else if ($_GET['method'] == 'delete') {
			$e->id = $_GET['id'];
			$e->populateById();
			$e->delete();
			
			$a->message = 'Deleted employee '.$e->name;
			$a->create();
		}
	}
	else {
		die('no permission');
	}
}

// vehicles
if ($_GET['table'] == 'vehicles') {
	$v = new Vehicle($db, $user);
	$vo = new VehicleOutput($db, $user);
	
	if ($user->level == 'Admin') {
		if ($_GET['method'] == 'modal') {
			if ($_GET['action'] == 'create') {
				$vo->modalCreate();
			}
			else if ($_GET['action'] == 'update') {
				$vo->fetchRow($_GET['id']);
				$vo->modalUpdate();
			}
		}
		else if ($_GET['method'] == 'create') {
			$v->populateByPost();
			$v->create();
			
			$a->message = 'Created vehicle '.$v->make.' '.$v->model.' '.$v->reg;
			$a->create();
			
			$vo->fetchRow($v->id);
			$vo->showRow();
		}
		else if ($_GET['method'] == 'update') {
			$v->id = $_GET['id'];
			$v->populateById();
			$a->before = array('make' => $v->make, 'model' => $v->model, 'type' => $v->type, 'reg' => $v->reg);
			$v->populateByPost();
			$a->after = array('make' => $v->make, 'model' => $v->model, 'type' => $v->type, 'reg' => $v->reg);
			$v->update();
			
			$a->message = 'Updated vehicle '.$v->make.' '.$v->model.' '.$v->reg;
			$a->create();
			
			$vo->fetchRow($v->id);
			$vo->showRow();
		}
		else if ($_GET['method'] == 'delete') {
			$v->id = $_GET['id'];
			$v->populateById();
			$v->delete();
			
			$a->message = 'Deleted vehicle '.$v->make.' '.$v->model.' '.$v->reg;
			$a->create();			
		}
	}
	else {
		die('no permission');
	}
}

// users
if ($_GET['table'] == 'users') {
	$u = new User($db, $user);
	$uo = new UserOutput($db, $user);
	
	if ($user->level == 'Admin') {
		if ($_GET['method'] == 'modal') {
			if ($_GET['action'] == 'create') {
				$uo->modalCreate();
			}
			else if ($_GET['action'] == 'update') {
				$uo->fetchRow($_GET['id']);
				$uo->modalUpdate();
			}
		}
		else if ($_GET['method'] == 'create') {
			$u->populateByPost();
			$u->create();	
			
			$a->message = 'Created user '.$u->username;
			$a->create();	
			
			$uo->fetchRow($u->id);
			$uo->showRow();
		}
		else if ($_GET['method'] == 'update') {			
			$u->id = $_GET['id'];
			$u->populateById();
			$a->before = array('username' => $u->username, 'level' => $u->level, 'permissions' => $u->permissions);
			$u->populateByPost();
			$a->after = array('username' => $u->username, 'level' => $u->level, 'permissions' => $u->permissions);
			$u->update();
			
			$a->message = 'Updated user '.$u->username;
			$a->create();
			
			$uo->fetchRow($u->id);
			$uo->showRow();
		}
		else if ($_GET['method'] == 'delete') {			
			$u->id = $_GET['id'];
			$u->populateById();
			$u->delete();
			
			$a->message = 'Deleted user '.$u->username;
			$a->create();
		}
	}
	else {
		die('no permission');
	}
}

// teams
if ($_GET['table'] == 'teams') {
	$l = new Location($db, $user);
	$t = new Team($db, $user);
	$to = new TeamOutput($db, $user);
	
	if ($_GET['method'] == 'create') {
		if ($user->level == 'Admin' || in_array('create_teams_'.$_GET['id_location'], $user->permissions)) {
			$l->id = $_GET['id_location'];
			$l->populateById();
			
			$t->id_location = $l->id;
			$t->date = $_GET['date'];
			$t->create();
			
			$a->message = 'Created '.$l->name.' team #'.$t->id_display.' for '.date('m/d/Y', $_GET['date']);
			$a->create();
			
			$to->date = $_GET['date'];
			$to->fetchTeam($t->id);
			$to->showTeam();
		}
		else {
			die('no permission');
		}
	}
	if ($_GET['method'] == 'copy') {
		if ($user->level == 'Admin' || in_array('create_teams_'.$_GET['id_location'], $user->permissions)) {
			$l->id = $_GET['id_location'];
			$l->populateById();
			
			$t->id_location = $l->id;
			$t->date = $_GET['date'];
			$t->copy();
			
			$a->message = 'Copied '.$l->name.' previous day teams for '.date('m/d/Y', $_GET['date']);
			$a->create();
			
			$to->timestamp = $_GET['date'];
			$to->fetchLocation($t->id_location);
			$to->showLocation();
		}
		else {
			die('no permission');
		}
	}
	else if ($_GET['method'] == 'modal') {
		if ($user->level == 'Admin' || in_array('view_teams_'.$_GET['id_location'], $user->permissions)) {
			if ($_GET['action'] == 'notes') {
				$to->fetchTeam($_GET['id']);
				$to->modalNotes();
			}
		}
		else {
			die('no permission');
		}
	}
	else if ($_GET['method'] == 'notes') {
		if ($user->level == 'Admin' || in_array('update_team_notes_'.$_POST['id_location'], $user->permissions)) {
			$l->id = $_POST['id_location'];
			$l->populateById();
			
			$t->id = $_GET['id'];
			$t->populateById();
			$a->before = $t->notes;
			$t->populateByPost();
			$a->after = $t->notes;
			$t->updateNotes();
			
			$a->message = 'Updated '.$l->name.' team #'.$t->id_display.' notes for '.date('m/d/Y', $t->date);
			$a->create();
			
			$to->fetchTeam($t->id);
			$to->showTeam();
		}
		else {
			die('no permission');
		}
	}
	else if ($_GET['method'] == 'delete') {
		if ($user->level == 'Admin' || in_array('delete_teams_'.$_GET['id_location'], $user->permissions)) {
			$l->id = $_GET['id_location'];
			$l->populateById();
			
			$t->id = $_GET['id'];
			$t->populateById();
			$t->delete();
			
			$a->message = 'Deleted '.$l->name.' team #'.$t->id_display.' for '.date('m/d/Y', $t->date);
			$a->create();
		}
		else {
			die('no permission');
		}
	}
	else if ($_GET['method'] == 'tabs') {
		$to->timestamp = $_GET['timestamp'];
		$to->showTabs($_GET['active_tab']);
	}
}

// team_employees
if ($_GET['table'] == 'team_employees') {
	$e = new Employee($db, $user);
	$l = new Location($db, $user);
	$t = new Team($db, $user);
	$te = new TeamEmployee($db, $user);
	$to = new TeamOutput($db, $user);	
	
	$e->id = $_GET['id_employee'];
	$e->populateById();
	$l->id = $_GET['id_location'];
	$l->populateById();			
	$t->id = $_GET['id_team'];
	$t->populateById();	
	
	if ($_GET['method'] == 'create') {
		if ($user->level == 'Admin' || in_array('create_team_objects_'.$_GET['id_location'], $user->permissions)) {	
			$te->id_team = $t->id;
			$te->id_employee = $e->id;
			
			if (!$te->exists()) {
				$te->create();
				
				$a->message = 'Added '.$e->name.' to '.$l->name.' team #'.$t->id_display.' for '.date('m/d/Y', $t->date);
				$a->create();
			}
			
			$to->fetchTeam($t->id);
			$to->showTeam();
		}
		else {
			die('no permission');
		}
	}
	else if ($_GET['method'] == 'delete') {
		if ($user->level == 'Admin' || in_array('delete_team_objects_'.$_GET['id_location'], $user->permissions)) {
			$te->id = $_GET['id'];
			$te->delete();
				
			$a->message = 'Deleted '.$e->name.' from '.$l->name.' team #'.$t->id_display.' for '.date('m/d/Y', $t->date);
			$a->create();
			
			$to->fetchTeam($t->id);
			$to->showTeam();
		}
		else {
			die('no permission');
		}
	}
}

// team_vehicles
if ($_GET['table'] == 'team_vehicles') {
	$v = new Vehicle($db, $user);
	$l = new Location($db, $user);
	$t = new Team($db, $user);
	$tv = new TeamVehicle($db, $user);
	$to = new TeamOutput($db, $user);	
	
	$v->id = $_GET['id_vehicle'];
	$v->populateById();
	$l->id = $_GET['id_location'];
	$l->populateById();			
	$t->id = $_GET['id_team'];
	$t->populateById();	
	
	if ($_GET['method'] == 'create') {
		if ($user->level == 'Admin' || in_array('create_team_objects_'.$_GET['id_location'], $user->permissions)) {	
			$tv->id_team = $t->id;
			$tv->id_vehicle = $v->id;
			
			if (!$tv->exists()) {
				$tv->create();
				
				$a->message = 'Added '.$v->make.' '.$v->model.' '.$v->reg.' to '.$l->name.' team #'.$t->id_display.' for '.date('m/d/Y', $t->date);
				$a->create();
			}
			
			$to->fetchTeam($t->id);
			$to->showTeam();
		}
		else {
			die('no permission');
		}
	}
	else if ($_GET['method'] == 'delete') {
		if ($user->level == 'Admin' || in_array('delete_team_objects_'.$_GET['id_location'], $user->permissions)) {
			$tv->id = $_GET['id'];
			$tv->delete();
				
			$a->message = 'Deleted '.$v->make.' '.$v->model.' '.$v->reg.' from '.$l->name.' team #'.$t->id_display.' for '.date('m/d/Y', $t->date);
			$a->create();
			
			$to->fetchTeam($t->id);
			$to->showTeam();
		}
		else {
			die('no permission');
		}
	}
}

?>