<!DOCTYPE html>
<html lang="en">
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title><?php echo $page['title'].' | '.APP_TITLE; ?></title>

		<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css">
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css">
		<link href="css/custom.css?version=<?php echo APP_VERSION; ?>" rel="stylesheet" type="text/css">

		<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
		<script src="js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="js/bootstrap.min.js" type="text/javascript"></script>
		<script src="js/purl.js" type="text/javascript"></script>
		<script src="js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="js/custom.js?version=<?php echo APP_VERSION; ?>" type="text/javascript"></script>

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js" type="text/javascript"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js" type="text/javascript"></script>
		<![endif]-->

	</head>
	<body class="modal-container">

		<nav class="navbar navbar-default navbar-static-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="dashboard.php" class="navbar-brand"><i class="fa fa-cog"></i> <?php echo APP_TITLE; ?></a>
				</div>

				<?php if ($user->is_signed_in) { ?>
					<div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							<li<?php if ($page['title'] == 'Dashboard') echo ' class="active"'; ?>><a href="dashboard.php">Dashboard</a></li>
							<li<?php if ($page['title'] == 'Teams') echo ' class="active"'; ?>><a href="teams.php">Teams</a></li>
							<li<?php if ($page['title'] == 'Training') echo ' class="active"'; ?>><a href="training.php">Training</a></li>
							<?php if ($user->level == 'Admin') { ?>
								<li<?php if ($page['title'] == 'Audits') echo ' class="active"'; ?>><a href="audits.php">Audits</a></li>
								<li<?php if ($page['title'] == 'Users') echo ' class="active"'; ?>><a href="users.php">Users</a></li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
										<i class="fa fa-cog"></i> Settings<span class="caret"></span>
									</a>
									<ul class="dropdown-menu">
										<li<?php if ($page['title'] == 'Locations') echo ' class="active"'; ?>><a href="locations.php">Locations</a></li>
										<li<?php if ($page['title'] == 'Employees') echo ' class="active"'; ?>><a href="employees.php">Employees</a></li>
										<li<?php if ($page['title'] == 'Vehicles') echo ' class="active"'; ?>><a href="vehicles.php">Vehicles</a></li>
										<li<?php if ($page['title'] == 'Licenses') echo ' class="active"'; ?>><a href="licenses.php">Licenses</a></li>
									</ul>
								</li>
							<?php } ?>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-user"></i> <?php echo $user->username; ?> <span class="caret"></span>
								</a>
								<ul class="dropdown-menu">
									<li><a href="sign-out.php">Sign Out</a></li>
								</ul>
							</li>
						</ul>
					</div>
				<?php } ?>
			</div>
		</nav>

		<div class="container-fluid">