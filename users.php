<?php

include 'includes/init.php';

if (!$user->is_signed_in || $user->level != 'Admin') {
	redirect('index.php');
}

$page['title'] = 'Users';

include 'header.php';

?>

<div class="page-header">
	<a href="ajax.php?table=users&method=modal&action=create" title="Create User" class="btn btn-primary pull-right modal-link"><i class="fa fa-plus"></i></a>
	<h1><?php echo $page['title']; ?></h1>
</div>

<?php

$lo = new UserOutput($db, $user);
$lo->fetchRows();
$lo->showTable();

include 'footer.php';

?>