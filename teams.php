<?php

include 'includes/init.php';

if (!$user->is_signed_in) {
	redirect('index.php');
}

if (isset($_GET['date'])) {
	$date = $_GET['date'];
	$timestamp = strtotime($_GET['date']);
}
else {
	redirect('teams.php?date='.date('m/d/Y', time()));
}

$page['title'] = 'Teams';

include 'header.php';

?>

<div class="page-header">
	<div class="form-inline pull-right">
		<a href="print.php?date=<?php echo date('m/d/Y', $timestamp); ?>" target="_blank" class="btn btn-primary hidden-xs" title="Print"><i class="fa fa-print"></i></a>
		<a href="teams.php?date=<?php echo date('m/d/Y', strtotime('-1 day', $timestamp)); ?>" class="btn btn-primary hidden-xs" title="Previous Day"><i class="fa fa-angle-double-left"></i></a>
		<input class="form-control datepicker" value="<?php echo $date; ?>">
		<a href="teams.php?date=<?php echo date('m/d/Y', strtotime('+1 day', $timestamp)); ?>" class="btn btn-primary hidden-xs" title="Next Day"><i class="fa fa-angle-double-right"></i></a>
	</div>
	<h1><?php echo $page['title']; ?> <small class="hidden-xs"><?php echo date('F j, Y', $timestamp); ?></small></h1>
</div>

<?php

$to = new TeamOutput($db, $user);
$to->setDateFields($date, $timestamp);

?>

<div class="row">
	<div class="col-md-9">
		<?php		
		$to->fetchLocations();
		$to->showLocations();
		?>
	</div>
	<div class="col-md-3">
		<?php		
		$to->showTabs();
		?>
	</div>
</div>

<?php

include 'footer.php';

?>